/*
 This file is DBHandler of this project.
 All operations regarding Database and DataWarehouse has been inplemented here
 */
package Karachi;

//import Businesslogic.courseregisteration;
import Karachi.Registration;
import Karachi.Student;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import net.java.frej.fuzzy.Fuzzy;

/**
 *
 * @author Abubaker
 */

public class DBHandler {

private static DBHandler instance;

    /**
     *
     */
    
private Connection con;
    public DBHandler()
  {
    
    
  }

   
    public Connection gettConnection() throws ClassNotFoundException, SQLException
    {
    Class.forName("com.mysql.jdbc.Driver"); 
    String host="jdbc:mysql://localhost:3306/mysql?useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
//   String host="jdbc:mysql://localhost:3306/test?useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
    String uName="root";
     String uPass="admin";
     Connection con = DriverManager.getConnection(host,uName,uPass);
     return con;
    }

    public int getUniqueValues(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT " + column + ") FROM " + table + "";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }
    
    public int getNoOfNull(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = null ";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }
        public int getNoOfCourses(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT " + column + ") FROM " + table + "";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }
        
            public int getNoOfMale(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = 'M'";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

                public int getNoOfFemale(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = 'F'";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

       public int getAverageStudentsPerSemester() throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT Semester) FROM dwhkarachi.registration";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        int totalSemester = rs.getInt(1);
        sql1 = "SELECT COUNT(DISTINCT SID,Degree) FROM dwhkarachi.registration";
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql1);
        rs.next();
        System.out.println(totalSemester+"-"+rs.getInt(1));
        return rs.getInt(1) / totalSemester;
    }

    public int getAverageStudentsPerBatch() throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT Batch) FROM dwhkarachi.student";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        int totalBatches = rs.getInt(1);
        sql1 = "SELECT COUNT(DISTINCT SID,Batch) FROM dwhkarachi.student";
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql1);
        rs.next();
          System.out.println(totalBatches+"-"+rs.getInt(1));      
        return rs.getInt(1) / totalBatches;
    }

        public String getUniqueStudentsRelationship() throws ClassNotFoundException, SQLException {
         Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT SID,Degree) FROM dwhkarachi.student";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        int noOfuniqIds = rs.getInt(1);
        sql1 = "SELECT COUNT(DISTINCT SID) FROM dwhkarachi.student";
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql1);
        rs.next();      
        int distinctIds = rs.getInt(1);
        return (String.valueOf(distinctIds)+"   and Total number of Students : "+String.valueOf(noOfuniqIds));
        
    }

     public int getPeopleMoreThan5Courses() throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT SID,Semester) FROM dwhkarachi.registration  ";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }
    public void addStudent(ArrayList<Student> slist) throws ClassNotFoundException, SQLException, InterruptedException
    {
        Connection conn=this.gettConnection();
        String sql1 = "insert into dwhkarachi.student(student_id, student_fname, student_lname, father_fname, father_lname, gender, address, birth_date, reg_date, reg_status, degree_status, last_degree, city) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";    
//        String sql1 = "insert into test.student(SID, Student_Fname, Student_Lname, Father_fname, Father_lname, DoB,Age, RegDate,Batch, Degree, Campus, LastDegree, RegStatus, DegreeStatus, Gender, Address) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";    
        PreparedStatement stmt =conn.prepareStatement(sql1);
       
        final int batchSize = slist.size();
        int count = 0;
       
        for(Student stu:slist)
        {   
            stmt.setString(1, stu.getSid());
            stmt.setString(2, stu.getFname());
            stmt.setString(3, stu.getLname());
            stmt.setString(4, stu.getFatheFname());
            stmt.setString(5, stu.getFatherLname());
            stmt.setString(6, Character.toString(stu.getGender()));
            stmt.setString(7,stu.getAddres());
            stmt.setDate(8, stu.getDob());
            stmt.setDate(9,stu.getRegdate());
            stmt.setString(10, Character.toString(stu.getRegstatus()));
            stmt.setString(11, Character.toString(stu.getDegreestatus()));
            stmt.setString(12, stu.getLastdegree());
            stmt.setString(13, stu.getCity());
//            stmt.setInt(14, stu.getBatch());
            stmt.addBatch();
            
             if(++count % batchSize == 0) {
             stmt.executeBatch();
             }
        }
try {
                       stmt.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }
        public void addCourseReg(ArrayList<Registration> rlist) throws ClassNotFoundException, SQLException {
            
        Connection conn=this.gettConnection();
        String sql1 = "insert into dwhkarachi.course_registrations(student_id, degree, semester, course_code, marks, descipline,grade) VALUES (?,?,?,?,?,?,?)";    
        PreparedStatement stmt =conn.prepareStatement(sql1);
       
        final int batchSize = rlist.size();
        int count = 0;
       
        for(Registration reg:rlist)
        {   
            stmt.setString(1, reg.getSid());
            stmt.setString(2, reg.getDegree());
            stmt.setString(3, reg.getSemester());
            stmt.setString(4, reg.getCourses());
            stmt.setInt(5, reg.getScore());
            stmt.setString(6, reg.getDiscpilie());
            stmt.setString(7, reg.getGrade());
            stmt.addBatch();
            
             if(++count % batchSize == 0) {
             stmt.executeBatch();
             }
        }
try {
                       stmt.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
            
    }
        public void populateCity(ArrayList<City> cty) throws ClassNotFoundException, SQLException
    {
        Connection con = this.gettConnection();
        String sql1 = "INSERT INTO dwhkarachi.city(city_name" + ") VALUES (?)";
        PreparedStatement stmt = con.prepareStatement(sql1);
        for(City c:cty)
        {
            stmt.setString(1, c.getCity().toLowerCase());
            stmt.addBatch();
        }
        try 
        {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }

    }
        public ArrayList<String> getCities() throws ClassNotFoundException, SQLException
    {
         Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT DISTINCT(city_name) From dwhkarachi.city";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        ArrayList<String> city = new ArrayList<>();
        while(rs.next())
        {
            city.add(rs.getString(1));
        }
        return city;
    }
        public void cityStandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       ArrayList <String> lis=new ArrayList<>();
       lis=this.getCities();
       Connection conn=this.gettConnection();
       
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwhkarachi.citylookup(standard_city,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(13),lis.get(i)))
           {
               stmt.setString(1, rs.getString(13).toLowerCase());
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
           }    
          }   
       }
        stmt.setString(1, "islamabad");
        stmt.setString(2, "isb");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rwp");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "raw");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpind");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpin");
        stmt.addBatch();
        stmt.setString(1, "abbotabad");
        stmt.setString(2, "abbottabad");
        stmt.addBatch();
        stmt.setString(1, "karachi");
        stmt.setString(2, "khr");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "dg Khan");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "d.g. khan");
        stmt.addBatch();
        stmt.executeBatch();
       
   }
        public void transformKarachi() throws ClassNotFoundException, SQLException, InterruptedException {

        Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT * From dwhkarachi.student";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        
        String sql2 = "INSERT INTO dwhkarachi.students(student_id, student_fname, student_lname, father_fname, father_lname, gender, address, birth_date, reg_date, reg_status, degree_status, last_degree, city" + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmtt = con1.prepareStatement(sql2);

        final int batchSize = 1000;
        int counter = 0;

        while (rs.next()) {
           
            stmtt.setString(1, rs.getString("student_id"));
//////////////
            
            String sqll="select standard_name from dwhkarachi.khi_standardnames where variation='"+rs.getString("student_fname").toLowerCase()+"'";
            Statement stm =con1.createStatement();
            ResultSet res=stm.executeQuery(sqll);
            res.next();
//          System.out.println(res.getString(1)+"<<");
            String sfname="NA";
            if(res.getRow()!=0)
            {
            sfname=res.getString(1);
            }
            
          //  stmtt.setString(2, sfname);
            stmtt.setString(2, rs.getString("student_fname"));
            stmtt.setString(3, rs.getString("student_lname"));
            stmtt.setString(4, rs.getString("father_fname"));
            stmtt.setString(5, rs.getString("father_lname"));
            stmtt.setString(6, rs.getString("gender"));
            stmtt.setString(7, rs.getString("address"));
           
            String sql="select standard_city from dwhkarachi.citylookup where variation='"+rs.getString(13)+"'";
            Statement stmm =con1.createStatement();
            ResultSet ress=stmm.executeQuery(sql);
            ress.next();
//          System.out.println(res.getString(1)+"<<");
            String City="karachi";
            if(ress.getRow()!=0)
            {
            City=ress.getString(1);
            }
           
            Date DOB = rs.getDate("birth_date");
            Date RD = rs.getDate("reg_date");

            if (RD.getYear() <= DOB.getYear()) {
                DOB.setYear(RD.getYear() - 18);
                stmtt.setDate(8, DOB);
            } else {
                stmtt.setDate(8, DOB);
            }
            stmtt.setDate(9, rs.getDate("reg_date"));
            stmtt.setString(10, rs.getString("reg_status"));
            stmtt.setString(11, rs.getString("degree_status"));
            
            stmtt.setString(12, rs.getString("last_degree"));
            
            stmtt.setString(13, City);
 //           stmtt.setInt(14, Integer.parseInt(rs.getString("batch")));
            stmtt.addBatch();

            if (++counter % batchSize == 0) {

                stmtt.executeBatch();
            }
        }
        try {

            stmtt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    
      //  this.createTransformedReg();
        
    }
        public void pupulateUniversityStudents(String tableName) throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   
           
        Connection con = this.gettConnection();
        String sql1 = "SELECT *,DATEDIFF(reg_date,birth_date) as AgeAtReg FROM "+tableName+"";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);
        // rs.next();        
        String sql2="Insert into university.students(student_id, student_fname, student_lname, father_fname, father_lname, gender, address, birth_date, reg_date, reg_status, degree_status, last_degree, city, age, batch, campus) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        String campus;
	int batch;
        while(rs.next())
        {
            stmt2.setString(1, rs.getString("student_id"));
            stmt2.setString(2, rs.getString("student_fname"));
            stmt2.setString(3, rs.getString("student_lname"));
            stmt2.setString(4, rs.getString("father_fname"));
            stmt2.setString(5, rs.getString("father_lname"));
            stmt2.setString(6, rs.getString("gender"));
            stmt2.setString(7,rs.getString("address"));
            stmt2.setDate(8, rs.getDate("birth_date"));
            stmt2.setDate(9,rs.getDate("reg_date"));
            batch=rs.getDate("reg_date").getYear();
            stmt2.setString(10, rs.getString("reg_status"));
            stmt2.setString(11, rs.getString("degree_status"));
            ///////////////// degree standardization ////////////////////  
               String input = rs.getString("last_degree");
               String alpha = input.replaceAll("[^a-zA-Z\\s]", "");
           // System.out.println(alpha.trim());
            java.util.StringTokenizer tokenizerr = new java.util.StringTokenizer(alpha.trim(), " ");
            StringBuilder temp = new StringBuilder();
            while (tokenizerr.hasMoreTokens()) {
                    temp.append(tokenizerr.nextToken());  
            }
            String last_degree = temp.toString();
        ///////////////////////////////////////////////////////////////////    
            stmt2.setString(12, last_degree.toLowerCase());
            stmt2.setString(13, rs.getString("city"));
           
            
            stmt2.setInt(14,rs.getInt(14)/365);
            stmt2.setInt(15, batch+1900);
 //           System.out.println(rs.getInt(15)/365);
            campus=rs.getString(1);
            if(campus.charAt(0)=='K')
            {
                stmt2.setString(16,"Karachi");
            }
            else if(campus.charAt(0)=='I')
            {
                stmt2.setString(16,"Islamabad");
            }
            else if(campus.charAt(0)=='L')
            {
                stmt2.setString(16,"Lahore");
            }
            else if(campus.charAt(0)=='P')
            {
                stmt2.setString(16,"Peshawar");                
            }
            stmt2.addBatch();
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }
        
        
         public void pupulateUniversityRegistration(String tableName) throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   
           String discipline;
             if(tableName.equals("dwh.course_registrations")||tableName.equals("dwhkarachi.course_registrations"))
             {
                discipline="descipline";
             }
             else
             {
                discipline="discipline";
             }
                        
             
        Connection con = this.gettConnection();
        String sql1 = "SELECT * FROM "+tableName+"";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);
         rs.next();        
        String sql2="Insert into university.course_registrations(student_id, degree, semester, course_code, marks, descipline, grade) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        String campus;
	int batch;
        while(rs.next())
        {
            stmt2.setString(1, rs.getString("student_id"));
            stmt2.setString(2, rs.getString("degree"));
            stmt2.setString(3, rs.getString("semester"));
            stmt2.setString(4, rs.getString("course_code"));
            stmt2.setInt(5, rs.getInt("marks"));
            
            stmt2.setString(6, rs.getString(discipline));
            stmt2.setString(7,rs.getString("grade"));
            
            stmt2.addBatch();
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }
        
        
        
        public ArrayList<String> getNames() throws ClassNotFoundException, SQLException
    {
         Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT DISTINCT(student_fname) From dwhkarachi.student";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        ArrayList<String> sfname = new ArrayList<>();
        while(rs.next())
        {
            sfname.add(rs.getString(1));
        }
        return sfname;
    }
        public void nameStandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       ArrayList <String> lis=new ArrayList<>();
       lis=this.getNames();
       Connection conn=this.gettConnection();
       
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwhkarachi.khi_standardnames(standard_name,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(2),lis.get(i)))
           {
               stmt.setString(1, rs.getString(2).toLowerCase());
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
        
           }    
          }
          
      }
        stmt.executeBatch();
       
   }
        
        ////////////////////////////// LOADING TO DWH ///////////////////////////
        
    public void populateDimentionCampus() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        String sql1 = "SELECT distinct campus FROM university.students";
//        String sql1 = "SELECT distinct campus FROM dwhkarachi.students";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);     
        String sql2="Insert into dimensional_modeling.campus(campus_id, campus_name, campus_province) VALUES (?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        String campus;
        
        while(rs.next())
        {
            campus=rs.getString(1);
            String province=" ";
             if(campus.charAt(0)=='K')
            {
               province="Sindh";
            }
            else if(campus.charAt(0)=='I')
            {
                province="ICT";
            }
            else if(campus.charAt(0)=='L')
            {
                province="Punjab";
            }
            else if(campus.charAt(0)=='P')
            {
                province="KPK";
            }
            stmt2.setString(1,campus+province);
            stmt2.setString(2,campus);
            stmt2.setString(3, province);
            stmt2.addBatch();
            
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }

    public void populateDimentionCourses() throws ClassNotFoundException, SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        String sql1 = "SELECT distinct course_code,descipline FROM university.course_registrations";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into dimensional_modeling.courses(course_id,course_code,course_type) VALUES (?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2);
       
        while(rs.next())
        {
            stmt2.setString(1,rs.getString(1)+rs.getString(2));
            stmt2.setString(2,rs.getString(1));
            stmt2.setString(3,rs.getString(2));           
            stmt2.addBatch();
           
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }

    public void populateDimentionDegree() throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
      String sql1 = "SELECT distinct degree,descipline FROM university.course_registrations";
//        String sql1 = "select distinct degree,descipline from dwhkarachi.course_registration";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into dimensional_modeling.degree(degree_id,degree_level,degree_program) VALUES (?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        
        while(rs.next())
        {
            stmt2.setString(1,rs.getString(1)+rs.getString(2));     
            stmt2.setString(2,rs.getString(1));
            stmt2.setString(3,rs.getString(2));
            stmt2.addBatch();
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }

    public void populateDimentionFAST() throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 // fact_id, student_id, course_id, campus_id, session_id, degree_id, marks, grade  
        Connection con = this.gettConnection();
        String sID;
        String campus=null;
        String province=null;
        String corsID;
        String sessID;
        String degrID=null;
        String desp;
        String term = null;
        int yr=0;

        String sql1="SELECT student_id,course_code,descipline,semester,degree,marks,grade FROM university.course_registrations";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);  

        String sql6="Insert into dimensional_modeling.fast( student_id, course_id, campus_id, session_id, degree_id, marks, grade, gpa) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement stmt6 =con.prepareStatement(sql6); 
        
           final int batchSize = 10000;
        int count = 0;
       
          
      //  rs.next();
        while(rs.next())
        {
        sID=rs.getString(1);
        
        if(sID.charAt(0)=='K')
        {
            campus="Karachi";
            province="Sindh";
        }
        else if(sID.charAt(0)=='I')
        {
            campus="Islamabad";
            province="ICT";
        }
        else if(sID.charAt(0)=='P')
        {
            campus="Peshawar";
            province="KPK";
        }
        else if(sID.charAt(0)=='L')
        {
            campus="Lahore";
            province="Punjab";
        }

        corsID=rs.getString(2)+rs.getString(3);
        desp=rs.getString(3);
        sessID=rs.getString(4);
        degrID=rs.getString(5);
            
        String camID=campus+province;
        //  getting student ID
        String sqlq="SELECT student_id FROM dimensional_modeling.students where student_id = '"+sID+"'";
        Statement stmnt = con.createStatement();
        ResultSet rs8 = stmnt.executeQuery(sqlq);
        rs8.next();
        if(rs8.getRow()!=0)
        {
//  getting campus ID
        String sql2="SELECT campus_id FROM dimensional_modeling.campus where campus_id = '"+camID+"'";
        Statement stmt2 = con.createStatement();
        ResultSet rs7 = stmt2.executeQuery(sql2);
        rs7.next();
//  getting course ID
        String sql3="SELECT course_id FROM dimensional_modeling.courses where course_id = '"+corsID+"'";
        Statement stmt3 = con.createStatement();
        ResultSet rs3 = stmt3.executeQuery(sql3);
        rs3.next();
//  getting Session ID
        String sql4="SELECT session_id FROM dimensional_modeling.session where session_id='"+sessID+"'";
        Statement stmt4 = con.createStatement();
        ResultSet rs4 = stmt4.executeQuery(sql4);
        rs4.next();
//        System.out.println(rs4.getString(1));
//  getting Degree ID
        String degree_id=degrID+desp;
        String sql5="SELECT degree_id FROM dimensional_modeling.degree where degree_id = '"+degree_id+"'";
        Statement stmt5 = con.createStatement();
        ResultSet rs5 = stmt5.executeQuery(sql5);
        rs5.next();
    //    System.out.println(rs.getString(1)+" "+rs3.getInt(1)+" "+rs2.getInt(1)+" "+rs4.getInt(1)+" "+rs.getInt(6)+" "+rs.getString(7));
            stmt6.setString(1, rs.getString(1));
            stmt6.setString(2, rs3.getString(1));
            stmt6.setString(3, rs7.getString(1));
            stmt6.setString(4, rs4.getString(1));
            stmt6.setString(5, rs5.getString(1));
            stmt6.setString(6, rs.getString(6));
            Double gpa=(Double.parseDouble(rs.getString(6))/100)*4;
            stmt6.setString(7, rs.getString(7));
            stmt6.setDouble(8,gpa);
            stmt6.addBatch();
            if (++count % batchSize == 0) {
               
                stmt6.executeBatch();
            }
            
            rs7.close();
            rs3.close();
            rs3.close();
            rs4.close();
            rs5.close();
            rs8.close();
            
          }
        }
        try {
                       stmt6.executeBatch();
                       
                } catch(BatchUpdateException e) {
                    System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(4)+" "+rs.getString(4)+" "+rs.getString(5));
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    
    }
    public void calculate_sgpa() throws ClassNotFoundException, SQLException
    {
         Connection con = this.gettConnection();
         String sql1 = "select F.student_id,F.session_id, SUM(F.gpa*3)/(count(F.course_id)*3) as SGPA  from dimensional_modeling.fast as F group by F.student_id, F.session_id;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.calculated_sgpa(student_id,session_id,sgpa) VALUES (?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setDouble(3, rs.getDouble(3));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
    }

     public void calculate_cgpa() throws ClassNotFoundException, SQLException
    {
        Connection con = this.gettConnection();
        String sql1 = "select student_id, (SUM(sgpa)/count(session_id)) as CGPA from business_questions.calculated_sgpa group by student_id;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.calculated_cgpa(student_id,cgpa) VALUES (?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
            stmt2.setString(1, rs.getString(1));
            
            stmt2.setDouble(2, rs.getDouble(2));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
    }
     
     
     public void cgpa_report() throws ClassNotFoundException, SQLException
    {
        Connection con = this.gettConnection();
        String sql1 = "select Distinct F.student_id,S.student_fname,S.student_lname,D.degree_program,S.reg_year,C.cgpa from business_questions.calculated_cgpa as C, dimensional_modeling.fast as F, dimensional_modeling.students as S, dimensional_modeling.degree as D where F.student_id=S.student_id and F.student_id=C.student_id and F.degree_id=D.degree_id order by D.degree_program,S.reg_year asc;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.cgpa_report(student_id,student_fname,student_lname,discipline,reg_year,cgpa) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, rs.getString(4));
            stmt2.setInt(5, rs.getInt(5)+1900);
            stmt2.setDouble(6, rs.getDouble(6));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
    }

     public void topPositionHolders() throws ClassNotFoundException, SQLException
    {
        Connection con = this.gettConnection();
        for(int i=1994;i<=2004;i++)
        {
        String sql1 = "select C.student_id,S.student_fname,S.student_lname, C.cgpa from dimensional_modeling.students as S, business_questions.calculated_cgpa as C where C.student_id=S.student_id and S.student_id like '%IBS%' and S.batch="+i+" group by S.batch,S.student_id order by C.cgpa desc LIMIT 3";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.top_positionholders(student_id,student_fname,student_lname,campus,batch,cgpa) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
          
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, "Islamabad");
            stmt2.setInt(5, i);
          //  System.out.println(rs.getDouble(4));
            stmt2.setDouble(6, rs.getDouble(4));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
        }
        for(int i=1994;i<=2004;i++)
        {
        String sql1 = "select C.student_id,S.student_fname,S.student_lname, C.cgpa from dimensional_modeling.students as S, business_questions.calculated_cgpa as C where C.student_id=S.student_id and S.student_id like '%KBS%' and S.batch="+i+" group by S.batch,S.student_id order by C.cgpa desc LIMIT 3;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.top_positionholders(student_id,student_fname,student_lname,campus,batch,cgpa) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
          
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, "Karachi");
            stmt2.setInt(5, i);
            stmt2.setDouble(6, rs.getDouble(4));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
        }
        for(int i=1994;i<=2004;i++)
        {
        String sql1 = "select C.student_id,S.student_fname,S.student_lname, C.cgpa from dimensional_modeling.students as S, business_questions.calculated_cgpa as C where C.student_id=S.student_id and S.student_id like '%LBS%' and S.batch="+i+" group by S.batch,S.student_id order by C.cgpa desc LIMIT 3;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.top_positionholders(student_id,student_fname,student_lname,campus,batch,cgpa) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
          
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, "Lahore");
            stmt2.setInt(5, i);
            stmt2.setDouble(6, rs.getDouble(4));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
        }
        for(int i=1994;i<=2004;i++)
        {
        String sql1 = "select C.student_id,S.student_fname,S.student_lname, C.cgpa from dimensional_modeling.students as S, business_questions.calculated_cgpa as C where C.student_id=S.student_id and S.student_id like '%PBS%' and S.batch="+i+" group by S.batch,S.student_id order by C.cgpa desc LIMIT 3;";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into business_questions.top_positionholders(student_id,student_fname,student_lname,campus,batch,cgpa) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        while(rs.next())
        {
          
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, "Peshawar");
            stmt2.setInt(5, i);
            stmt2.setDouble(6, rs.getDouble(4));
            stmt2.addBatch();
        }
        try {
              stmt2.executeBatch();           
            } catch(BatchUpdateException e) 
                {
                    System.out.println(e.getMessage());
                }
        }
    
    }

     
     
    public void populateDimentionGeography() throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           Connection con = this.gettConnection();
        String sql1 = "SELECT student_id,city,address FROM university.students";
//        String sql1 = "SELECT student_id,city,address FROM dwhkarachi.students";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into dimensional_modeling.geography(geography_id,students_id,city,street,house) VALUES (?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        String addrs;
        String[] token;
        int i=1;
        while(rs.next())
        {
            stmt2.setInt(1, i);
            stmt2.setString(2, rs.getString(1));
            stmt2.setString(3, rs.getString(2));
            addrs=rs.getString(3);
            token = addrs.split(" "); //     String[] token;
            stmt2.setString(4,token[4]+token[5]+token[6]);
            stmt2.setString(5,token[0]+token[1]+token[3]);  
            stmt2.addBatch();
            i++;
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    
    }

    public  void populateDimentionsession() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          Connection con = this.gettConnection();
        String sql1 = "SELECT distinct semester FROM university.course_registrations";
//        String sql1 = "SELECT distinct semester FROM dwhkarachi.course_registration";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);       
        String sql2="Insert into dimensional_modeling.session(session_id,term,year) VALUES (?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        String smstr;
       
        while(rs.next())
        {
             smstr= rs.getString(1);
            stmt2.setString(1,smstr);
           // System.out.println(smstr);
            if(smstr.contains("Fall"))
            {
                stmt2.setString(2, "Fall");
                if(smstr.charAt(4)=='0')
                {
                stmt2.setInt(3,Integer.valueOf(smstr.substring(4))+2000);
                }
                else
                {
                stmt2.setInt(3,Integer.valueOf(smstr.substring(4))+1900);                    
                }
            }
            else if(smstr.contains("Spring"))
            {
               // System.out.println(smstr);
                stmt2.setString(2, "Spring");
                if(smstr.charAt(6)=='0')
                {
                stmt2.setInt(3,Integer.valueOf(smstr.substring(6))+2000);                    
                }
                else {
                stmt2.setInt(3,Integer.valueOf(smstr.substring(6))+1900);                    
                }
            } else if(smstr.contains("Sum"))
            {
                stmt2.setString(2, "Summer"); 
                if(smstr.charAt(6)=='0')
                {
                 stmt2.setInt(3,Integer.valueOf(smstr.substring(6))+2000);                   
                }
                else
                {
                stmt2.setInt(3,Integer.valueOf(smstr.substring(6))+1900);                    
                }
            }
            stmt2.addBatch();
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }

    public void populateDimentionStudent() throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        String sql1 = "SELECT * FROM university.students";
//        String sql1 = "SELECT * FROM dwhkarachi.students";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);      
        String sql2="Insert into dimensional_modeling.students(student_id, student_fname, student_lname, father_fname, father_lname, gender, reg_status, degree_status, last_degree, age_at_reg, batch,reg_year,reg_month,reg_day,birth_year,birth_month,birth_day,address,city) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2); 
        
        while(rs.next())
        {
            
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, rs.getString(4));
            stmt2.setString(5, rs.getString(5));
            stmt2.setString(6, rs.getString(6));
            stmt2.setString(7, rs.getString(10));
            stmt2.setString(8, rs.getString(11));
            stmt2.setString(9, rs.getString(12));
            stmt2.setInt(10,rs.getInt(14));
            stmt2.setInt(11,rs.getInt(15));
            stmt2.setInt(12,rs.getDate(9).getYear());
            stmt2.setInt(13,rs.getDate(9).getMonth());
            stmt2.setInt(14,rs.getDate(9).getDay());
            stmt2.setInt(15,rs.getDate(8).getYear());
            stmt2.setInt(16,rs.getDate(8).getMonth());
            stmt2.setInt(17,rs.getDate(8).getDay());
            stmt2.setString(18, rs.getString(7));
            stmt2.setString(19, rs.getString(13));
            
            stmt2.addBatch();
            
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }

    public void populateDimentiontime() throws ClassNotFoundException, SQLException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
               Connection con = this.gettConnection();
        String sql1 = "SELECT student_id,reg_date FROM university.students";
//      String sql1 = "SELECT student_id,reg_date FROM dwhkarachi.students";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);
        String sql2="Insert into dimensional_modeling.time(time_id,student_id,date_type,year,month,week) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt2 =con.prepareStatement(sql2);
        int i=1;
        while(rs.next())
        {
            stmt2.setInt(1, i);
            stmt2.setString(2, rs.getString(1));
            stmt2.setString(3,"Reg");
            stmt2.setInt(4,(rs.getDate(2).getYear()+1900));
//            System.out.print(rs.getString(1)+","+rs.getDate(2).getYear()+","+rs.getDate(2).getMonth()+"/");
            stmt2.setInt(5, rs.getDate(2).getMonth()+1);  // for street number
            stmt2.setInt(6, rs.getDate(2).getDate()/7);  // for house number
            stmt2.addBatch();
            i++;
        }
        try {
                       stmt2.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
        
        String sql3= "SELECT student_id,birth_date FROM university.students";
        Statement stmt3 = con.createStatement();
        ResultSet rs1 = stmt3.executeQuery(sql3);        
        String sql4="Insert into dimensional_modeling.time(time_id,student_id,date_type,year,month,week) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt4 =con.prepareStatement(sql4);
        
        while(rs1.next())
        {
            stmt4.setInt(1, i);
            stmt4.setString(2, rs1.getString(1));
            stmt4.setString(3,"DOB");
            stmt4.setInt(4,(rs1.getDate(2).getYear()+1900));
            stmt4.setInt(5, rs1.getDate(2).getMonth()+1);  
            stmt4.setInt(6, rs1.getDate(2).getDate()/7);  
            stmt4.addBatch();
            i++;
        }
        try {
                       stmt4.executeBatch();
                       
                } catch(BatchUpdateException e) {
                        //you should handle exception for failed records here
                        System.out.println(e.getMessage());
                }
    }
    
     public void businessQ1CommonAddress() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        String sql1 = "SELECT a.student_id,a.student_fname,b.student_id,b.student_lname,a.address FROM dimensional_modeling.students a,dimensional_modeling.students b where a.address=b.address AND a.student_id<b.student_id;";
//        String sql1 = "SELECT distinct campus FROM dwhkarachi.students";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);
        String sql2 = "Insert into business_questions.common_address(first_Student_Rollnum, first_Student_Name, second_Student_Rollnum, second_Student_Name, address) VALUES (?,?,?,?,?)";
        PreparedStatement stmt2 = con.prepareStatement(sql2);

        while (rs.next()) {
            stmt2.setString(1, rs.getString(1));
            stmt2.setString(2, rs.getString(2));
            stmt2.setString(3, rs.getString(3));
            stmt2.setString(4, rs.getString(4));
            stmt2.setString(5, rs.getString(5));
//            System.out.println(rs.getString(1));
            stmt2.addBatch();
        }
        try {
            stmt2.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void businessQ2AttractedCourses() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        String sql1 = "select distinct course_id,count(course_id) from dimensional_modeling.fast group by course_id order by count(course_id) desc";
        Statement stmt1 = con.createStatement();
        ResultSet rs = stmt1.executeQuery(sql1);
        String sql2 = "Insert into business_questions.attracted_courses(course_id,Course_count) VALUES (?,?)";
        PreparedStatement stmt2 = con.prepareStatement(sql2);

        while (rs.next()) {
            stmt2.setString(1, rs.getString(1));
            System.out.println(rs.getString(1));
            System.out.println(rs.getString(2));
            stmt2.setInt(2, Integer.valueOf(rs.getString(2)));
            stmt2.addBatch();
        }
        try {
            stmt2.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void businessQ3BackgroundRatio() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();

//        String sql1 = "select count(*) from dimensional_modeling.students";
//        Statement stmt1 = con.createStatement();
//        ResultSet rs = stmt1.executeQuery(sql1);        
//        rs.next();
//        int totalStudents=Integer.valueOf(rs.getString(1));
// 
        String sql2 = "select student_id,last_degree,count(*) as ratio from dimensional_modeling.students group by last_degree";
        Statement stmt2 = con.createStatement();
        ResultSet rs2 = stmt2.executeQuery(sql2);
        String sql3 = "Insert into business_questions.background_ratio(dept,background_degrees,ratios) VALUES (?,?,?)";
        PreparedStatement stmt3 = con.prepareStatement(sql3);
        String dept;
        while (rs2.next()) {
            dept = rs2.getString(1).substring(1, 3);
            stmt3.setString(1, dept);
            stmt3.setString(2, rs2.getString(2));
            stmt3.setInt(3, Integer.valueOf(rs2.getString(3)));
//          System.out.println((Integer.valueOf(rs2.getString(2)))/totalStudents);
            stmt3.addBatch();
        }
        try {
            stmt3.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void businessQ4AvgAgeCampusWise() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();

        String sql2 = "select C.campus_name,avg(S.age_at_reg)as Average from dimensional_modeling.students as S, dimensional_modeling.fast as F,dimensional_modeling.campus as C where S.student_id=F.student_id AND F.campus_id=C.campus_id group by C.campus_name";
        Statement stmt2 = con.createStatement();
        ResultSet rs2 = stmt2.executeQuery(sql2);
        String sql3 = "Insert into business_questions.Campus_Age(Campus_Name,Average_age) VALUES (?,?)";
        PreparedStatement stmt3 = con.prepareStatement(sql3);
        while (rs2.next()) {
            stmt3.setString(1, rs2.getString(1));
            stmt3.setString(2, rs2.getString(2));
//            stmt3.setDouble(2, Double.valueOf(rs2.getString(2)));
            stmt3.addBatch();
        }
        try {
            stmt3.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void businessQ5NameMostUsed() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();
        //getting student fname       
        String sql2 = "select Distinct student_fname,count(student_fname) from dimensional_modeling.students group by student_fname order by count(student_fname) desc LIMIT 1";
        Statement stmt2 = con.createStatement();
        ResultSet rs2 = stmt2.executeQuery(sql2);
        //getting Student lname       
        String sql3 = "select Distinct student_lname,count(student_lname) from dimensional_modeling.students group by student_lname order by count(student_lname) desc LIMIT 1";
        Statement stmt3 = con.createStatement();
        ResultSet rs3 = stmt3.executeQuery(sql3);
        //getting father fname       
        String sql4 = "select Distinct father_fname,count(father_fname) from dimensional_modeling.students group by father_fname order by count(father_fname) desc LIMIT 1";
        Statement stmt4 = con.createStatement();
        ResultSet rs4 = stmt4.executeQuery(sql4);
        //getting father lname       
        String sql5 = "select Distinct father_lname,count(father_lname) from dimensional_modeling.students group by father_lname order by count(father_lname) desc LIMIT 1";
        Statement stmt5 = con.createStatement();
        ResultSet rs5 = stmt5.executeQuery(sql5);

        String sql1 = "Insert into business_questions.name_most_used(name_type,name,name_count) VALUES (?,?,?)";
        PreparedStatement stmt1 = con.prepareStatement(sql1);
        int i = 0;
        while (rs2.next() || rs3.next() || rs4.next() || rs5.next()) {
            if (i == 0) {
                stmt1.setString(1, "Student First Name");
                stmt1.setString(2, rs2.getString(1));
                stmt1.setString(3, rs2.getString(2));
            } else if (i == 1) {
                stmt1.setString(1, "Student last Name");
                stmt1.setString(2, rs3.getString(1));
                stmt1.setString(3, rs3.getString(2));
            } else if (i == 2) {
                stmt1.setString(1, "Father First Name");
                stmt1.setString(2, rs4.getString(1));
                stmt1.setString(3, rs4.getString(2));
            } else if (i == 3) {
                stmt1.setString(1, "Father Last Name");
                stmt1.setString(2, rs5.getString(1));
                stmt1.setString(3, rs5.getString(2));
            }
            i++;
//            stmt3.setDouble(2, Double.valueOf(rs2.getString(2)));
            stmt1.addBatch();
        }
        try {
            stmt1.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void businessQ6TransfersYearWise() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();

        String sql2 = "select batch,count(reg_status) from dimensional_modeling.students  where reg_status='T' group by reg_year desc";
        Statement stmt2 = con.createStatement();
        ResultSet rs2 = stmt2.executeQuery(sql2);
        String sql3 = "Insert into business_questions.transfer_cases(batch,count) VALUES (?,?)";
        PreparedStatement stmt3 = con.prepareStatement(sql3);
        while (rs2.next()) {
            stmt3.setInt(1, Integer.valueOf(rs2.getString(1)));
            stmt3.setInt(2, Integer.valueOf(rs2.getString(2)));
//            stmt3.setDouble(2, Double.valueOf(rs2.getString(2)));
            stmt3.addBatch();
        }
        try {
            stmt3.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }
    
// select a.student_id,b.student_id,a.student_fname from dimensional_modeling.students a,dimensional_modeling.students b where a.student_id<b.student_id AND a.student_fname=b.student_fname AND a.student_lname=b.student_lname AND a.father_fname=b.father_fname AND a.father_lname=b.father_lname AND a.birth_year=b.birth_year AND a.birth_day=b.birth_day;
    public void businessQ7MultipleDegrees() throws ClassNotFoundException, SQLException {
//       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Connection con = this.gettConnection();

        String sql2 = "select a.student_id,b.student_id,a.student_fname from dimensional_modeling.students a,dimensional_modeling.students b where a.student_id<b.student_id AND a.student_fname=b.student_fname AND a.student_lname=b.student_lname AND a.father_fname=b.father_fname AND a.father_lname=b.father_lname AND a.birth_year=b.birth_year AND a.birth_day=b.birth_day";
        Statement stmt2 = con.createStatement();
        ResultSet rs2 = stmt2.executeQuery(sql2);
        String sql3 = "Insert into business_questions.multiple_degrees(student_id,student_other_degree_id,student_name) VALUES (?,?,?)";
        PreparedStatement stmt3 = con.prepareStatement(sql3);
        while (rs2.next()) {
            stmt3.setString(1, (rs2.getString(1)));
            stmt3.setString(2, (rs2.getString(2)));
            stmt3.setString(3, (rs2.getString(3)));
//          stmt3.setDouble(2, Double.valueOf(rs2.getString(2)));
            stmt3.addBatch();
        }
        try {
            stmt3.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    
}


