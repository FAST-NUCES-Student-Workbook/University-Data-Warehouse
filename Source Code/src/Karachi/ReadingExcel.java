/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Karachi;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Abubaker
 */
public class ReadingExcel {

    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException, IOException {
        // TODO code application logic here


    }
    
        public static void karachiExtraction(String path) throws IOException, FileNotFoundException, ClassNotFoundException, SQLException, InterruptedException {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   
        ArrayList<String> fileName = new ArrayList<>();
        fileName.add(path.concat("/Reg_BS_KHR.xls"));
        fileName.add(path.concat("/Reg_MS_KHR.xls"));
         getStudentDataKarachi(path);
         System.out.println("Student table populated");
         getRegistrationDataKarachi(fileName);
         System.out.println("Registration table populated");
//          dataProfiling("Karachi");
  //       dataTransformKarachi();
        }


    public  void dataTransformKarachi() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException, InterruptedException
    {
        DBHandler dbh = new DBHandler();
            String inputLine;
            BufferedReader br= new BufferedReader(new FileReader(new File("G:\\cities.txt")));
            ArrayList<City> cty = new ArrayList<>();
            while((inputLine = br.readLine()) != null)
            {
                City c = new City(inputLine);
                cty.add(c);
            }
            dbh.populateCity(cty);
            
            dbh.cityStandardization("dwhkarachi.student");
            dbh.nameStandardization("dwhkarachi.student");
            dbh.transformKarachi();
        
    }
    public static void dataProfiling(String campus) throws ClassNotFoundException, SQLException
    {
        if(campus.equals("Karachi"))
        {
             DBHandler dbh = new DBHandler();
//            ///*********** UNIQUE VALUES  **********///
//            System.out.println("///*********** UNIQUE VALUES  **********//");
//            System.out.println("For Student:");
//            System.out.println("No. of Unique Values of SID: " + dbh.getUniqueValues("dwhkarachi.student", "SID"));
//            System.out.println("No. of Unique Values of Student First Name: " + dbh.getUniqueValues("dwhkarachi.student", "Student_Fname"));
//            System.out.println("No. of Unique Values of Student Last Name: " + dbh.getUniqueValues("dwhkarachi.student", "Student_Lname"));
//            System.out.println("No. of Unique Values of Father First Name: " + dbh.getUniqueValues("dwhkarachi.student", "Father_fname"));
//            System.out.println("No. of Unique Values of Father Last Name: " + dbh.getUniqueValues("dwhkarachi.student", "Father_lname"));
//            System.out.println("No. of Unique Values of DateOfBirth: " + dbh.getUniqueValues("dwhkarachi.student", "DoB"));
//            System.out.println("No. of Unique Values of RegDate: " + dbh.getUniqueValues("dwhkarachi.student", "RegDate"));
//            System.out.println("No. of Unique Values of RegStatus: " + dbh.getUniqueValues("dwhkarachi.student", "RegStatus"));
//            System.out.println("No. of Unique Values of Last Degree: " + dbh.getUniqueValues("dwhkarachi.student", "LastDegree"));
//            System.out.println("No. of Unique Values of DegreeStatus: " + dbh.getUniqueValues("dwhkarachi.student", "DegreeStatus"));
//            System.out.println("No. of Unique Values of Gender: " + dbh.getUniqueValues("dwhkarachi.student", "Gender"));
//            System.out.println("No. of Unique Values of Address: " + dbh.getUniqueValues("dwhkarachi.student", "Address"));
//            
//            /////////// for Registration ///////////
//            System.out.println("For Registration:");
//            System.out.println("No. of Unique Values of SID: " + dbh.getUniqueValues("dwhkarachi.registration", "SID"));
//            System.out.println("No. of Unique Values of Degree: " + dbh.getUniqueValues("dwhkarachi.registration", "Degree"));
//            System.out.println("No. of Unique Values of Course: " + dbh.getUniqueValues("dwhkarachi.registration", "Courses"));
//            System.out.println("No. of Unique Values of Score: " + dbh.getUniqueValues("dwhkarachi.registration", "Score"));
//            System.out.println("No. of Unique Values of Semester: " + dbh.getUniqueValues("dwhkarachi.registration", "Semester"));
//            System.out.println("No. of Unique Values of Discpiline: " + dbh.getUniqueValues("dwhkarachi.registration", "Discpiline"));
         
//            ///*********** NULL VALUES  **********///
//            System.out.println("///*********** NULL VALUES  **********//");
//            System.out.println("For Student:");        
//            System.out.println("No. of Null Values of SID: " + dbh.getNoOfNull("dwhkarachi.student", "SID"));
//            System.out.println("No. of Null Values of Student First Name: " + dbh.getNoOfNull("dwhkarachi.student", "Student_Fname"));
//            System.out.println("No. of Null Values of Student Last Name: " + dbh.getNoOfNull("dwhkarachi.student", "Student_Lname"));
//            System.out.println("No. of Null Values of Father First Name: " + dbh.getNoOfNull("dwhkarachi.student", "Father_fname"));
//            System.out.println("No. of Null Values of Father Last Name: " + dbh.getNoOfNull("dwhkarachi.student", "Father_lname"));
//            System.out.println("No. of Null Values of DateOfBirth: " + dbh.getNoOfNull("dwhkarachi.student", "DoB"));
//            System.out.println("No. of Null Values of RegDate: " + dbh.getNoOfNull("dwhkarachi.student", "RegDate"));
//            System.out.println("No. of Null Values of RegStatus: " + dbh.getNoOfNull("dwhkarachi.student", "RegStatus"));
//            System.out.println("No. of Null Values of Last Degree: " + dbh.getNoOfNull("dwhkarachi.student", "LastDegree"));
//            System.out.println("No. of Null Values of DegreeStatus: " + dbh.getNoOfNull("dwhkarachi.student", "DegreeStatus"));
//            System.out.println("No. of Null Values of Gender: " + dbh.getNoOfNull("dwhkarachi.student", "Gender"));
//            System.out.println("No. of Null Values of Address: " + dbh.getNoOfNull("dwhkarachi.student", "Address"));
//           /////////// for Registration ///////////
//            System.out.println("For Registration:");
//            System.out.println("No. of Null Values of SID: " + dbh.getNoOfNull("dwhkarachi.registration", "SID"));
//            System.out.println("No. of Null Values of Degree: " + dbh.getNoOfNull("dwhkarachi.registration", "Degree"));
//            System.out.println("No. of Null Values of Course: " + dbh.getNoOfNull("dwhkarachi.registration", "Courses"));
//            System.out.println("No. of Null Values of Score: " + dbh.getNoOfNull("dwhkarachi.registration", "Score"));
//            System.out.println("No. of Null Values of Semester: " + dbh.getNoOfNull("dwhkarachi.registration", "Semester"));
//            System.out.println("No. of Null Values of Discpiline: " + dbh.getNoOfNull("dwhkarachi.registration", "Discpiline"));
 
//            ///*********** INVALID VALUES  **********///
//            System.out.println("///*********** INVALID VALUES  **********//");
//            System.out.println("For Student:");        
//            System.out.println("  Invalid Values of SID: " + dbh.getInvalidValues("dwhkarachi.student", "SID"));
//            System.out.println("  Invalid Values of Student First Name: " + dbh.getInvalidValues("dwhkarachi.student", "Student_Fname"));
//            System.out.println("  Invalid Values of Student Last Name: " + dbh.getInvalidValues("dwhkarachi.student", "Student_Lname"));
//            System.out.println("  Invalid Values of Father First Name: " + dbh.getInvalidValues("dwhkarachi.student", "Father_fname"));
//            System.out.println("  Invalid Values of Father Last Name: " + dbh.getInvalidValues("dwhkarachi.student", "Father_lname"));
//            System.out.println("  Invalid Values of DateOfBirth: " + dbh.getInvalidValues("dwhkarachi.student", "DoB"));
//            System.out.println("  Invalid Values of RegDate: " + dbh.getInvalidValues("dwhkarachi.student", "RegDate"));
//            System.out.println("  Invalid Values of RegStatus: " + dbh.getInvalidValues("dwhkarachi.student", "RegStatus"));
//            System.out.println("  Invalid Values of Last Degree: " + dbh.getInvalidValues("dwhkarachi.student", "LastDegree"));
//            System.out.println("  Invalid Values of DegreeStatus: " + dbh.getInvalidValues("dwhkarachi.student", "DegreeStatus"));
//            System.out.println("  Invalid Values of Gender: " + dbh.getInvalidValues("dwhkarachi.student", "Gender"));
//            System.out.println("  Invalid Values of Address: " + dbh.getInvalidValues("dwhkarachi.student", "Address"));
//           /////////// for Registration ///////////
//            System.out.println("For Registration:");
//            System.out.println("  Invalid Values of SID: " + dbh.getInvalidValues("dwhkarachi.registration", "SID"));
//            System.out.println("  Invalid Values of Degree: " + dbh.getInvalidValues("dwhkarachi.registration", "Degree"));
//            System.out.println("  Invalid Values of Course: " + dbh.getInvalidValues("dwhkarachi.registration", "Courses"));
//            System.out.println("  Invalid Values of Score: " + dbh.getInvalidValues("dwhkarachi.registration", "Score"));
//            System.out.println("  Invalid Values of Semester: " + dbh.getInvalidValues("dwhkarachi.registration", "Semester"));
//            System.out.println("  Invalid Values of Discpiline: " + dbh.getInvalidValues("dwhkarachi.registration", "Discpiline"));
             
//            ///*********** Total Courses  **********///
//            System.out.println("///*********** Total Number of Courses  **********//");
//            System.out.println("Total No Of Courses: " + dbh.getNoOfCourses("dwhkarachi.registration", "Courses"));             
             
//           ///*********** Total Number of Female and Male Students  **********///
//            System.out.println("///*********** Total Number of Female and Male Students  **********//");        
//            //No of MALE Students
//            System.out.println("Total No of Male Students: " + dbh.getNoOfMale("dwhkarachi.student", "Gender"));
//            //No of Female Students
//            System.out.println("Total No of Female Students: " + dbh.getNoOfFemale("dwhkarachi.student", "Gender"));
        
              ///*********** Total no of people who has taken more than 5 courses in a semester  **********///
// not implemented yet              System.out.println("Total no of people who has taken more than 5 courses in a semester   " + dbh.getPeopleMoreThan5Courses());
             ///*********** The relationship between total no. of unique student ID’s and total no. of students **********///
//          System.out.println("Relationship between Total Number of Unique Students ID's are : " + dbh.getUniqueStudentsRelationship());
             ///***********  Average no of people per semester of each campus  **********///
//            System.out.println("No of Average Students per Semester in KARACHI campus: " + dbh.getAverageStudentsPerSemester());
            ///*********** Average no of students in every batch of each campus  **********///
//            System.out.println("No of Average Students per Batch per Campus: " + dbh.getAverageStudentsPerBatch());
        }
    }
public static Calendar ConverttoDate1(String d,String format) throws ParseException
{
            Calendar mydate = new GregorianCalendar();    
            java.util.Date thedate = new SimpleDateFormat(format, Locale.ENGLISH).parse(d);
            mydate.setTime(thedate);
            return mydate;
}
  
    public static void getStudentDataKarachi(String path) {
        DBHandler db = new DBHandler();
        path=path.concat("/Student.xls");
        ArrayList<Student> stdObjAlist = new ArrayList<>();
        String sname = null;
        String sidtem=null;
        String fname=null;
        String degreestatus = null;
        String degree=null;
        String regstatus = null;
 //       String[] token = null;
        try {
            FileInputStream file = new FileInputStream(new File(path));

            //Create Workbook instance holding reference to .xlsx file
 //           HSSFWorkbook workbook = new HSSFWorkbook(file);
            org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);
            System.out.println(workbook.getNumberOfSheets()-1);
            //Get first/desired sheet from the workbook

            for (int i = 0; i < workbook.getNumberOfSheets() - 1; i++) {
     //           HSSFSheet sheet = workbook.getSheetAt(i);
                org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(i);
                if (workbook.getSheetName(i).startsWith("MS")) {
                    degree="MS";
                } else if (workbook.getSheetName(i).startsWith("BS")) {
                    degree="BS";
                }

                //Iterate through each rows and column one by one
                Iterator<Row> rowIterator = sheet.iterator();
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    Student stdObj = new Student();
                    //For each row, iterate through all the columns
                    if (rowIterator.hasNext() && row.getRowNum() != 0) {
                        sidtem=row.getCell(0).toString();
                        stdObj.setSid("K"+degree+"-"+sidtem);
                        sname = row.getCell(1).toString();

            int count=0;
            java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(sname, " ");
            StringBuilder lname = new StringBuilder();
            while (tokenizer.hasMoreTokens()) {
               
                if(count==0)
                {
                    sname=tokenizer.nextToken();
                    count++;
                }
                else
                {
                    lname.append(tokenizer.nextToken()+" ");
                    count++;
                }
              
            }
            
            stdObj.setFname(sname);
            stdObj.setLname(lname.toString());
           
//                        token = name.split(" ");
//                       if(token.length ==2 && token[0].length()==0)
//                        {
//                           stdObj.setFname(token[1]);
//                           stdObj.setLname(token[0]);
//                        }else if (token.length ==2 && token[0].length()!=0) {
//                            stdObj.setFname(token[0]);
//                            stdObj.setLname(token[1]);
//                        } else if ( token.length == 3) {
//                            stdObj.setFname(token[0] );
//                            stdObj.setLname(token[1]+" "+token[2]);
//                        } else if ( token.length == 4) {
//                            stdObj.setFname(token[0] );
//                            stdObj.setLname(token[1]+ " " +  token[2] + " " + token[3]);
//                        } else if ( token.length == 5) {
//                            stdObj.setFname(token[0] );
//                            stdObj.setLname(token[1]+ " " +  token[2] + " " + token[3] + " " + token[4]);
//                        } 

                        fname=null;
                        fname=row.getCell(2).toString();
                        count=0;
            java.util.StringTokenizer tokenizerfather = new java.util.StringTokenizer(fname, " ");
            StringBuilder flname = new StringBuilder();
            while (tokenizerfather.hasMoreTokens()) {
                if(count==0)
                {
                    fname=tokenizerfather.nextToken();
                    count++;
                }
                else
                {
                    flname.append(tokenizerfather.nextToken()+" ");
                    count++;
                }
              
            }
            stdObj.setFatheFname(fname);
            stdObj.setFatherLname(flname.toString());

            String d=row.getCell(3).toString();
   
            if(d.contains("-"))
            {
                String format="dd-MMM-yy";
                Calendar mydate=ConverttoDate1(d,format);
                int year=mydate.get(Calendar.YEAR)-1900;
                int month=mydate.get(Calendar.MONTH);
                int day=mydate.get(Calendar.DAY_OF_MONTH);
                Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
                stdObj.setDob(DateOfBirth);
            }else if(d.contains("/"))
            {
                String format="dd/MMM/yy";
                Calendar mydate=ConverttoDate1(d,format);
                int year=mydate.get(Calendar.YEAR)-1900;
                int month=mydate.get(Calendar.MONTH);
                int day=mydate.get(Calendar.DAY_OF_MONTH);
                Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
                stdObj.setDob(DateOfBirth);
            }
                        stdObj.setGender(row.getCell(4).toString().charAt(0));
//                      stdObj.setRegdate(row.getCell(5).toString());
            String dd=row.getCell(5).toString();
 //                           System.out.println(dd);
            if(dd.contains("-"))
            {
                String format="dd-MMM-yy";
                Calendar mydate=ConverttoDate1(dd,format);
                int year=mydate.get(Calendar.YEAR)-1900;
                int month=mydate.get(Calendar.MONTH);
                int day=mydate.get(Calendar.DAY_OF_MONTH);
                Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
                stdObj.setRegdate(DateOfBirth);
//                stdObj.setBatch((mydate.get(Calendar.YEAR)));
            }else if(dd.contains("/"))
            {
                String format="dd/MMM/yy";
                Calendar mydate=ConverttoDate1(dd,format);
                int year=mydate.get(Calendar.YEAR)-1900;
                int month=mydate.get(Calendar.MONTH);
                int day=mydate.get(Calendar.DAY_OF_MONTH);
                Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
                stdObj.setRegdate(DateOfBirth);
  //              stdObj.setBatch(mydate.get(Calendar.YEAR));   
            }
                        stdObj.setLastdegree(row.getCell(9).toString());
                        stdObj.setRegstatus(row.getCell(6).toString().charAt(0));

                        stdObj.setDegreestatus(row.getCell(7).toString().charAt(0));

                        stdObj.setAddres(row.getCell(8).toString());
                        ArrayList<String> cty = new ArrayList<>();
                        java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(stdObj.getAddres(), " ");
                        while(tokenizerr3.hasMoreTokens())
                        {
                            cty.add(tokenizerr3.nextToken());
                        }
                        stdObj.setCity(cty.get(cty.size()-1).toLowerCase());
                        
                        stdObjAlist.add(stdObj);
                    }
                    else
                    {
                        continue;
                    }
                }  //end row while iterator
            }  // end for loop     
             db.addStudent(stdObjAlist);
           // sending data of one sheet to the database
            file.close();
        } // end try
        catch (Exception e) {
            e.printStackTrace();
        }
    }
  
    public static void getRegistrationDataKarachi(ArrayList fileName) {
        DBHandler db = new DBHandler();
        ArrayList<Registration> regObjAlist = new ArrayList<>();        
        String degree = null;

        String[] token = null;
        try {
            for (Object fileName1 : fileName) {
                FileInputStream file = new FileInputStream(new File((String) fileName1));
                //Create Workbook instance holding reference to .xlsx file
                HSSFWorkbook workbook = new HSSFWorkbook(file);
                System.out.println(workbook.getNumberOfSheets()-1);
                token = fileName1.toString().split("_");
                if (token[1].equals("BS")) {
                    degree="BS";
                } else if (token[1].equals("MS")) {
                    degree="MS";
                }
                //Get first/desired sheet from the workbook
                for (int i = 0; i < workbook.getNumberOfSheets() - 1; i++) {
                    HSSFSheet sheet = workbook.getSheetAt(i);

                    //Iterate through each rows and column one by one
                    Iterator<Row> rowIterator = sheet.iterator();
                    while (rowIterator.hasNext()) {
                        Row row = rowIterator.next();
                        Registration regObj = new Registration();
                        //For each row, iterate through all the columns
                        if (rowIterator.hasNext()&& row.getRowNum()!=0) {
                            
                            String sidtem=row.getCell(0).toString();
                            regObj.setSid("K"+degree+"-"+sidtem);
                            regObj.setDegree(degree);
                            regObj.setCourses(row.getCell(1).toString());
                            double score=Double.parseDouble(row.getCell(2).toString());
                            regObj.setScore((int)score);
                            regObj.setSemester(row.getCell(3).toString());
                            regObj.setDiscpilie(row.getCell(4).toString());
                            regObj.setGrade(grade_calculate((int)score));
                            //                           System.out.println("");
                            regObjAlist.add(regObj);
                            //                           System.out.println(workbook.getSheetAt(i));
                        }
                        else
                        {
                            continue;
                        }  
                    }  //end row while iterator
                }  // end for loop     
                file.close();
            }
         db.addCourseReg(regObjAlist);
        } // end try
        catch (Exception e) {
            e.printStackTrace();
        }
    }
     public static String grade_calculate(int marks) throws ClassNotFoundException, SQLException
{
       String grade="C";
       
       if(marks>80)
       {
          grade="A+";
       }
       if(marks<80 && marks>75)
       {
          grade="A";
       }
       if(marks<75 && marks>70)
       {
           grade="A-";
       }
       if(marks<70 && marks>67)
       {
          grade="B+";
       }
       if(marks<67 && marks>60)
       {
           grade="B";
       }
       if(marks<60 && marks>55)
       {
          grade="B-";
       }
        if(marks<55 && marks>50)
       {
          grade="C+";
       }
         if(marks<55 && marks>53)
       {
          grade="C";
       }
         if(marks<53 && marks>50)
       {
          grade="C-";
       }
         if(marks<45 && marks>40)
       {
           grade="D+";
       }
         if(marks<40 && marks>38)
       {
           grade="D";
       }
       
        if(marks<38)
       {
           grade="F";
       }
       return grade;
    }


}
