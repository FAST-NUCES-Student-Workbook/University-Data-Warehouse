/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Karachi;

/**
 *
 * @author Abubaker
 */
public class Registration {
private String sid;
private String courses;
private int score;
private String Degree;
private String semester;
private String discpilie;
private String grade;

    public Registration(String sid, String courses, int score, String Degree, String semester, String discpilie, String grade) {
        this.sid = sid;
        this.courses = courses;
        this.score = score;
        this.Degree = Degree;
        this.semester = semester;
        this.discpilie = discpilie;
        this.grade=grade;
    }

    public Registration() {
    }

    public String getSid() {
        return sid;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    
    public String getCourses() {
        return courses;
    }

    public int getScore() {
        return score;
    }

    public String getDegree() {
        return Degree;
    }

    public String getSemester() {
        return semester;
    }

    public String getDiscpilie() {
        return discpilie;
    }

    // setters
    public void setSid(String sid) {
        this.sid = sid;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setDegree(String Degree) {
        this.Degree = Degree;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setDiscpilie(String discpilie) {
        this.discpilie = discpilie;
    }
    
}
