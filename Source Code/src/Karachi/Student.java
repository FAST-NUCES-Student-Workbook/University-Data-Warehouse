/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Karachi;

import java.sql.Date;

/**
 *
 * @author sibghat
 */
public class Student {
private String sid;
private String fname;
private String lname;
private String fatheFname;
private String fatherLname;
private String addres;
private Date dob;
private Date regdate;
private char gender;
private String lastdegree;
private char regstatus;
private char degreestatus;
private String city;

    public Student() {
    }

    public Student(String sid, String fname, String lname, String fatheFname, String fatherLname, String addres, Date dob, Date regdate, char gender, String lastdegree, char regstatus, char degreestatus, String city) {
        this.sid = sid;
        this.fname = fname;
        this.lname = lname;
        this.fatheFname = fatheFname;
        this.fatherLname = fatherLname;
        this.addres = addres;
        this.dob = dob;
        this.regdate = regdate;
        this.gender = gender;
        this.lastdegree = lastdegree;
        this.regstatus = regstatus;
        this.degreestatus = degreestatus;
        this.city = city;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFatheFname() {
        return fatheFname;
    }

    public void setFatheFname(String fatheFname) {
        this.fatheFname = fatheFname;
    }

    public String getFatherLname() {
        return fatherLname;
    }

    public void setFatherLname(String fatherLname) {
        this.fatherLname = fatherLname;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getRegdate() {
        return regdate;
    }

    public void setRegdate(Date regdate) {
        this.regdate = regdate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getLastdegree() {
        return lastdegree;
    }

    public void setLastdegree(String lastdegree) {
        this.lastdegree = lastdegree;
    }

    public char getRegstatus() {
        return regstatus;
    }

    public void setRegstatus(char regstatus) {
        this.regstatus = regstatus;
    }

    public char getDegreestatus() {
        return degreestatus;
    }

    public void setDegreestatus(char degreestatus) {
        this.degreestatus = degreestatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city.toLowerCase();
    }

}
