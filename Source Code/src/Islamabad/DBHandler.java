/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Islamabad;

import java.sql.*;
import java.util.ArrayList;
import net.java.frej.fuzzy.Fuzzy;

/**
 *
 * @author Aqeel's
 */
public class DBHandler {

    private Connection con;

    public DBHandler() throws ClassNotFoundException, SQLException {

    }

    public Connection gettConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String host = "jdbc:mysql://localhost:3306/mysql?useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
        String uName = "root";
        String uPass = "admin";
        Connection con = DriverManager.getConnection(host, uName, uPass);
        return con;
    }

    public void addStudent(ArrayList<Student> slist) throws ClassNotFoundException, SQLException, InterruptedException {

        Connection con = this.gettConnection();

        String sql1 = "INSERT INTO dwh.student(ID,student_fname,father_fname,RegDate,RegStatus,DegreeStatus,DateOfBirth,Education,Gender,Address,Batch,City,student_lname,father_lname" + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = con.prepareStatement(sql1);

        final int batchSize = 10000;
        int count = 0;
        
         

        for (Student stu : slist) {
            stmt.setString(1, stu.getID());
            stmt.setString(2, stu.getStudent_fname());
            stmt.setString(3, stu.getFather_fname());
            stmt.setDate(4, stu.getRegDate());
            stmt.setString(5, Character.toString(stu.getRegStatus()));
            stmt.setString(6, Character.toString(stu.getDegreeStatus()));
            stmt.setDate(7, stu.getDateOfBirth());
            stmt.setString(8, stu.getEducation());
            stmt.setString(9, Character.toString(stu.getGender()));
            stmt.setString(10, stu.getAddress());
            stmt.setInt(11, stu.getBatch());
            stmt.setString(12, stu.getCity());
            stmt.setString(13, stu.getStudent_lname());
            stmt.setString(14, stu.getFather_lname());
            stmt.addBatch();

            if (++count % batchSize == 0) {
                stmt.executeBatch();
            }

//              Statement stmt =con.createStatement();
//        String sql1 = "INSERT INTO dwh.student(ID,Name,Father,RegDate,RegStatus,DegreeStatus,DateOfBirth,Education,Gender,Address,Degree"+") VALUES ('" +stu.getID()+  "', '" +stu.getName()+   "', '" +stu.getFather()+  "', '" +stu.getRegDate() +  "', '" + stu.getRegStatus() +  "', '" + stu.getDegreeStatus()+  "', '" + stu.getDateOfBirth() +  "', '" + stu.getEducation()+  "', '" + stu.getGender()+  "', '" + stu.getAddress()+"', '" + stu.getDegree()+ "')";    
//        
        }
        try {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }

    }

    public void addRegistration(ArrayList<Registration> rlist) throws ClassNotFoundException, SQLException, InterruptedException {

        Connection con = this.gettConnection();
        String sql1 = "INSERT INTO dwh.registration(ID,Course,Marks,Discipline,Session,Degree" + ") VALUES (?,?,?,?,?,?)";

        PreparedStatement stmt = con.prepareStatement(sql1);

        final int batchSize = 100000;
        int count = 0;
        for (Registration r : rlist) {
            stmt.setString(1, r.getID());
            stmt.setString(2, r.getCourse());
            stmt.setInt(3, r.getMarks());
            stmt.setString(4, r.getDiscipline());
            stmt.setString(5, r.getSession());
            stmt.setString(6, r.getDegree());

            stmt.addBatch();

            if (++count % batchSize == 0) {
                stmt.executeBatch();
            }
        }
        try {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }

    }
    
    public void createTransformedReg() throws ClassNotFoundException, SQLException, InterruptedException {

       Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT Distinct ID,Session,COURSE,marks,Discipline,Degree From dwh.registration";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        
        String sql2 = "INSERT INTO dwh.course_registrations(student_id, degree, semester, course_code, marks, descipline,grade" + ") VALUES (?,?,?,?,?,?,?)";

        PreparedStatement stmtt = con1.prepareStatement(sql2);

        final int batchSize = 10000;
        int counter = 0;

        while (rs.next()) {
          stmtt.setString(1, rs.getString(1));
                 stmtt.setString(2, rs.getString(6));
                 stmtt.setString(3, rs.getString(2));
                 stmtt.setString(4, rs.getString(3));
                 stmtt.setInt(5, rs.getInt(4));
                 stmtt.setString(6, rs.getString(5));
                 stmtt.setString(7, this.grade_calculate(rs.getInt(4)));
           
                 stmtt.addBatch();

                if (++counter % batchSize == 0) {

                stmtt.executeBatch();
            }
        }
        stmtt.executeBatch();

    }
    public String grade_calculate(int marks) throws ClassNotFoundException, SQLException
{
       String grade="C";
       
       if(marks>80)
       {
          grade="A+";
       }
       if(marks<80 && marks>75)
       {
          grade="A";
       }
       if(marks<75 && marks>70)
       {
           grade="A-";
       }
       if(marks<70 && marks>67)
       {
          grade="B+";
       }
       if(marks<67 && marks>60)
       {
           grade="B";
       }
       if(marks<60 && marks>55)
       {
          grade="B-";
       }
        if(marks<55 && marks>50)
       {
          grade="C+";
       }
         if(marks<55 && marks>53)
       {
          grade="C";
       }
         if(marks<53 && marks>50)
       {
          grade="C-";
       }
         if(marks<45 && marks>40)
       {
           grade="D+";
       }
         if(marks<40 && marks>38)
       {
           grade="D";
       }
       
        if(marks<38)
       {
           grade="F";
       }
       return grade;
    }

    public int getUniqueValues(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT " + column + ") FROM " + table + "";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getNoOfNull(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = null ";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getNoOfCourses(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT " + column + ") FROM " + table + "";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getNoOfMale(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = 'M'";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getNoOfFemale(String table, String column) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT count(*) FROM " + table + " WHERE " + column + " = 'F'";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getUniqueStudents(String table) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT student_fname, father_fname, RegDate, RegStatus, DegreeStatus, DateOfBirth, Education, Gender, Address) FROM " + table + "";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1);
    }

    public int getAverageStudentsPerSemester(String table, String semester,String student_id) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT "+semester+") FROM "+table+"";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        int totalSemester = rs.getInt(1);
        sql1 = "SELECT COUNT(DISTINCT "+student_id+") FROM "+table+"";
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1) / totalSemester;
    }

    public int getAverageStudentsPerBatch(String table, String batch,String student_id) throws ClassNotFoundException, SQLException {
        Connection con = this.gettConnection();
        String sql1 = "SELECT COUNT(DISTINCT "+batch+") FROM "+table+"";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        rs.next();
        int totalBatches = rs.getInt(1);
        sql1 = "SELECT COUNT(DISTINCT "+student_id+") FROM "+table+"";
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql1);
        rs.next();
        return rs.getInt(1) / totalBatches;
    }
    
    public ArrayList<String> getCities() throws ClassNotFoundException, SQLException
    {
         Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT DISTINCT(city_name) From dwh.city";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        ArrayList<String> city = new ArrayList<>();
        while(rs.next())
        {
            city.add(rs.getString(1));
        }
        return city;
    }
    
    public ArrayList<String> getNames() throws ClassNotFoundException, SQLException
    {
         Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT DISTINCT(student_fname) From dwh.student";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        ArrayList<String> sfname = new ArrayList<>();
        while(rs.next())
        {
            sfname.add(rs.getString(1));
        }
        return sfname;
    }
    
    public void populateCity(ArrayList<City> cty) throws ClassNotFoundException, SQLException
    {
        Connection con = this.gettConnection();
        String sql1 = "INSERT INTO dwh.city(city_name" + ") VALUES (?)";
        PreparedStatement stmt = con.prepareStatement(sql1);
        for(City c:cty)
        {
            stmt.setString(1, c.getCity().toLowerCase());
            stmt.addBatch();
        }
        try 
        {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }

    }

    public void transformIslamabad() throws ClassNotFoundException, SQLException, InterruptedException {

        Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT * From dwh.student";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        
        String sql2 = "INSERT INTO dwh.students(student_id, student_fname, student_lname, father_fname, father_lname, gender, address, birth_date, reg_date, reg_status, degree_status, last_degree, city" + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmtt = con1.prepareStatement(sql2);

        final int batchSize = 1000;
        int counter = 0;

        while (rs.next()) {
           
            stmtt.setString(1, rs.getString("ID"));
//////////////
            
            String sqll="select standard_name from dwh.isb_standardnames where variation='"+rs.getString("student_fname").toLowerCase()+"'";
            Statement stm =con1.createStatement();
            ResultSet res=stm.executeQuery(sqll);
            res.next();
//          System.out.println(res.getString(1)+"<<");
            String sfname="NA";
            if(res.getRow()!=0)
            {
            sfname=res.getString(1);
            }
            
            stmtt.setString(2, sfname);
           // stmtt.setString(2, rs.getString("student_fname"));
            stmtt.setString(3, rs.getString("student_lname"));
            stmtt.setString(4, rs.getString("father_fname"));
            stmtt.setString(5, rs.getString("father_lname"));
            stmtt.setString(6, rs.getString("Gender"));
            stmtt.setString(7, rs.getString("Address"));
            
            String sql="select standard_city from dwh.citylookup where variation='"+rs.getString("City").toLowerCase()+"'";
            Statement stmm =con1.createStatement();
            ResultSet ress=stmm.executeQuery(sql);
            ress.next();
//          System.out.println(res.getString(1)+"<<");
            String City="islamabad";
            if(ress.getRow()!=0)
            {
            City=ress.getString(1);
            }
            
            Date DOB = rs.getDate("DateOfBirth");
            Date RD = rs.getDate("RegDate");

            if (RD.getYear() <= DOB.getYear()) {
                DOB.setYear(RD.getYear() - 18);
                stmtt.setDate(8, DOB);
            } else {
                stmtt.setDate(8, DOB);
            }
            stmtt.setDate(9, rs.getDate("RegDate"));
            stmtt.setString(10, rs.getString("RegStatus"));
            stmtt.setString(11, rs.getString("DegreeStatus"));
            stmtt.setString(12, rs.getString("Education"));
            stmtt.setString(13, City);
           // stmtt.setString(counter, sql);
            stmtt.addBatch();

            if (++counter % batchSize == 0) {

                stmtt.executeBatch();
            }
        }
        try {

            stmtt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    
        this.createTransformedReg();
        
    }
    
    public void cleanTables()
    {
        
    }
    public void cityStandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       ArrayList <String> lis=new ArrayList<>();
       lis=this.getCities();
       Connection conn=this.gettConnection();
       
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwh.citylookup(standard_city,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(12),lis.get(i)))
           {
               stmt.setString(1, rs.getString(12).toLowerCase());
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
           }    
          }   
       }
        stmt.setString(1, "islamabad");
        stmt.setString(2, "isb");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rwp");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "raw");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpind");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpin");
        stmt.addBatch();
        stmt.setString(1, "abbotabad");
        stmt.setString(2, "abbottabad");
        stmt.addBatch();
        stmt.setString(1, "karachi");
        stmt.setString(2, "khi");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "dg Khan");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "d.g. khan");
        stmt.addBatch();
        stmt.executeBatch();
       
   }
    
    public void nameStandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       ArrayList <String> lis=new ArrayList<>();
       lis=this.getNames();
       Connection conn=this.gettConnection();
       
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwh.isb_standardnames(standard_name,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(2),lis.get(i)))
           {
               stmt.setString(1, rs.getString(2).toLowerCase());
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
        
           }    
          }
          
      }
        stmt.executeBatch();
       
   }
    
}



//}
