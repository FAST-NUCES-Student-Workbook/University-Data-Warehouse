/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Islamabad;

/**
 *
 * @author Aqeel's
 */
public class Registration {

    
    String Course;
    int Marks;
    String Discipline;
    String Session;
    String Degree;
    String ID;

    public Registration() {
    }

    public Registration(String Course, int Marks, String Discipline, String Session, String Degree, String ID) {
        
        this.Course = Course;
        this.Marks = Marks;
        this.Discipline = Discipline;
        this.Session = Session;
        this.Degree = Degree;
        this.ID = ID;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String Degree) {
        this.Degree = Degree;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
   
    public String getCourse() {
        return Course;
    }

    public void setCourse(String Course) {
        this.Course = Course;
    }

    public int getMarks() {
        return Marks;
    }

    public void setMarks(int Marks) {
        this.Marks = Marks;
    }

    public String getDiscipline() {
        return Discipline;
    }

    public void setDiscipline(String Discipline) {
        this.Discipline = Discipline;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String Session) {
        this.Session = Session;
    }
    
    
    
    
}
