/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Islamabad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author Aqeel's
 */
public class FSMS {

    public FSMS() {
    }

    public void insertStudent(String path) throws ClassNotFoundException, SQLException, InterruptedException {

        try {

            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            Connection conn = DriverManager.getConnection("jdbc:ucanaccess://"+path+"\\ISB.mdb");

            Statement st = conn.createStatement();

            String sql = "Select * from BS_Students";

            ResultSet rs = st.executeQuery(sql);

            ArrayList<Student> slist = new ArrayList<>();

            while (rs.next()) {

                String Name = rs.getString("Name");
            String student_fname="NA";
            int count1=0;
            java.util.StringTokenizer tokenizerr = new java.util.StringTokenizer(Name, " ");
            StringBuilder slname = new StringBuilder();
            while (tokenizerr.hasMoreTokens()) {
               
                if(count1==0)
                {
                    student_fname=tokenizerr.nextToken();
                    count1++;
                }
                else
                {
                    slname.append(tokenizerr.nextToken()+" ");
                    count1++;
                }
              
            }
            String student_lname = slname.toString();
                
                String Father = rs.getString("Father");
                
                 
            String father_fname="NA";
            int count2=0;
            java.util.StringTokenizer tokenizerr1 = new java.util.StringTokenizer(Father, " ");
            StringBuilder flname = new StringBuilder();
            while (tokenizerr1.hasMoreTokens()) {
               
                if(count2==0)
                {
                    father_fname=tokenizerr1.nextToken();
                    count2++;
                }
                else
                {
                    flname.append(tokenizerr1.nextToken()+" ");
                    count2++;
                }
              
            }
            String father_lname = flname.toString();
            
            
             if (student_fname.isEmpty()) {
                student_fname="NA";
            } 
            if (student_lname.isEmpty()) {
                student_lname="NA";
            }
             if (father_fname.isEmpty()) {
                father_fname="NA";
            } 
            if (father_lname.isEmpty()) {
                father_lname="NA";
            } 
                
                char RegStatus = rs.getString("Reg Status").charAt(0);
                char DegreeStatus = rs.getString("Degree Status").charAt(0);
                String Education = rs.getString("Education");
                String Address = rs.getString("Address");
                
                 
             ArrayList<String> cty = new ArrayList<>();
             java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(Address, " ");
             while(tokenizerr3.hasMoreTokens())
             {
                 cty.add(tokenizerr3.nextToken());
             }
            String City=cty.get(cty.size()-1).toLowerCase();
            
                Calendar mydate = new GregorianCalendar();
                String mystring = rs.getString("Date of Birth");
                java.util.Date thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(mystring);
                mydate.setTime(thedate);
                int year = mydate.get(Calendar.YEAR) - 1900;
                int month = mydate.get(Calendar.MONTH);
                int day = mydate.get(Calendar.DAY_OF_MONTH);

                Date DateOfBirth = new Date(year, month, day);//rs.getString("Date of Birth");

                mystring = rs.getString("Reg Date");
                thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(mystring);
                mydate.setTime(thedate);
                year = mydate.get(Calendar.YEAR) - 1900;
                month = mydate.get(Calendar.MONTH);
                day = mydate.get(Calendar.DAY_OF_MONTH);
                int Batch = mydate.get(Calendar.YEAR);
                Date RegDate = new Date(year, month, day);
                String ID = "IBS" + "-" + rs.getString("Roll Num");
                int G = Integer.parseInt(rs.getString("Gender"));
                char Gender = '1';
                if (G == 1) {
                    Gender = 'M';
                } else if (G == 0) {
                    Gender = 'F';
                }

            // System.out.println(rs.getString(1)+rs.getString(2)+rs.getString(3));
                // Student stdnt= new Student(ID, Name, Father, RegStatus, DegreeStatus, Education, Address, Degree, RegDate, DateOfBirth, Gender);
                Student std = new Student(ID, student_fname, student_lname, father_fname, father_lname, RegStatus, DegreeStatus, Education, Address, RegDate, DateOfBirth, Gender, Batch, City);

                slist.add(std);

            }

            sql = "Select * from MS_Students";

            rs = st.executeQuery(sql);

            while (rs.next()) {
 String Name = rs.getString("Name");
            String student_fname="NA";
            int count1=0;
            java.util.StringTokenizer tokenizerr = new java.util.StringTokenizer(Name, " ");
            StringBuilder slname = new StringBuilder();
            while (tokenizerr.hasMoreTokens()) {
               
                if(count1==0)
                {
                    student_fname=tokenizerr.nextToken();
                    count1++;
                }
                else
                {
                    slname.append(tokenizerr.nextToken()+" ");
                    count1++;
                }
              
            }
            String student_lname = slname.toString();
                
                String Father = rs.getString("Father");
                
                 
            String father_fname="NA";
            int count2=0;
            java.util.StringTokenizer tokenizerr1 = new java.util.StringTokenizer(Father, " ");
            StringBuilder flname = new StringBuilder();
            while (tokenizerr1.hasMoreTokens()) {
               
                if(count2==0)
                {
                    father_fname=tokenizerr1.nextToken();
                    count2++;
                }
                else
                {
                    flname.append(tokenizerr1.nextToken()+" ");
                    count2++;
                }
              
            }
            String father_lname = flname.toString();
               
            
            if (student_fname.isEmpty()) {
                student_fname="NA";
            } 
            if (student_lname.isEmpty()) {
                student_lname="NA";
            }
             if (father_fname.isEmpty()) {
                father_fname="NA";
            } 
            if (father_lname.isEmpty()) {
                father_lname="NA";
            } 
                
                char RegStatus = rs.getString("Reg Status").charAt(0);
                char DegreeStatus = rs.getString("Degree Status").charAt(0);
                String Education = rs.getString("Education");
                String Address = rs.getString("Address");
                
                 ArrayList<String> cty = new ArrayList<>();
             java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(Address, " ");
             while(tokenizerr3.hasMoreTokens())
             {
                 cty.add(tokenizerr3.nextToken());
             }
            String City=cty.get(cty.size()-1).toLowerCase();

                Calendar mydate = new GregorianCalendar();
                String mystring = rs.getString("Date of Birth");
                java.util.Date thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(mystring);
                mydate.setTime(thedate);
                int year = mydate.get(Calendar.YEAR) - 1900;
                int month = mydate.get(Calendar.MONTH);
                int day = mydate.get(Calendar.DAY_OF_MONTH);
                int Batch = mydate.get(Calendar.YEAR);
                Date DateOfBirth = new Date(year, month, day);//rs.getString("Date of Birth");

                mystring = rs.getString("Reg Date");
                thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(mystring);
                mydate.setTime(thedate);
                year = mydate.get(Calendar.YEAR) - 1900;
                month = mydate.get(Calendar.MONTH);
                day = mydate.get(Calendar.DAY_OF_MONTH);

                Date RegDate = new Date(year, month, day);
                //System.out.println("Date of : "+DateOfBirth);
                String ID = "IMS" + "-" + rs.getString("Roll Num");
                int G = Integer.parseInt(rs.getString("Gender"));
                char Gender = '1';
                if (G == 1) {
                    Gender = 'M';
                } else if (G == 0) {
                    Gender = 'F';
                }
                // System.out.println(rs.getString(1)+rs.getString(2)+rs.getString(3));
                Student stdnt = new Student(ID, student_fname, student_lname, father_fname, father_lname, RegStatus, DegreeStatus, Education, Address, RegDate, DateOfBirth, Gender, Batch, City);

                slist.add(stdnt);

            }

            DBHandler dbh = new DBHandler();
            dbh.addStudent(slist);

        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }

    }

    public void insertRegistration(String path) throws ClassNotFoundException, SQLException, InterruptedException {

        try {

            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            Connection conn = DriverManager.getConnection("jdbc:ucanaccess://"+path+"\\ISB.mdb");

            Statement st = conn.createStatement();

            String sql = "Select * from Registration";

            ResultSet rs = st.executeQuery(sql);

            ArrayList<Registration> rlist = new ArrayList<>();

            while (rs.next()) {

                String Course = rs.getString("Course");
                String Discipline = rs.getString("Discipline");
                String Session = rs.getString("Session");
                int Marks = Integer.parseInt(rs.getString("Marks"));
                String Degree = null;
                if (Discipline.charAt(0) == 'M') {
                    Degree = "MS";
                } else {
                    Degree = "BS";
                }

                String ID = "I" + Degree + "-" + rs.getString("Roll Num");

                Registration rg = new Registration(Course, Marks, Discipline, Session, Degree, ID);

                rlist.add(rg);

            }

            DBHandler dbh = new DBHandler();
            dbh.addRegistration(rlist);

        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }

    }

    public void dataProfiling(String campus) throws ClassNotFoundException, SQLException {
        if (campus.equals("Islamabad")) {
            DBHandler dbh = new DBHandler();
            //UNIQUE VALUES
            System.out.println("For Students:");
            System.out.println("Unique Values of ID: " + dbh.getUniqueValues("dwh.student", "ID"));
            System.out.println("Unique Values of Name: " + dbh.getUniqueValues("dwh.student", "student_fname"));
            System.out.println("Unique Values of Father: " + dbh.getUniqueValues("dwh.student", "father_fname"));
            System.out.println("Unique Values of RegDate: " + dbh.getUniqueValues("dwh.student", "RegDate"));
            System.out.println("Unique Values of RegStatus: " + dbh.getUniqueValues("dwh.student", "RegStatus"));
            System.out.println("Unique Values of DegreeStatus: " + dbh.getUniqueValues("dwh.student", "DegreeStatus"));
            System.out.println("Unique Values of DateOfBirth: " + dbh.getUniqueValues("dwh.student", "DateOfBirth"));
            System.out.println("Unique Values of Education: " + dbh.getUniqueValues("dwh.student", "Education"));
            System.out.println("Unique Values of Gender: " + dbh.getUniqueValues("dwh.student", "Gender"));
            System.out.println("Unique Values of Address: " + dbh.getUniqueValues("dwh.student", "Address"));
            //NULL VALUES
            System.out.println("No Of Null Values of ID: " + dbh.getNoOfNull("dwh.student", "ID"));
            System.out.println("No Of Null Values of Name: " + dbh.getNoOfNull("dwh.student", "student_fname"));
            System.out.println("No Of Null Values of Father: " + dbh.getNoOfNull("dwh.student", "father_fname"));
            System.out.println("No Of Null Values of RegDate: " + dbh.getNoOfNull("dwh.student", "RegDate"));
            System.out.println("No Of Null Values of RegStatus: " + dbh.getNoOfNull("dwh.student", "RegStatus"));
            System.out.println("No Of Null Values of DegreeStatus: " + dbh.getNoOfNull("dwh.student", "DegreeStatus"));
            System.out.println("No Of Null Values of DateOfBirth: " + dbh.getNoOfNull("dwh.student", "DateOfBirth"));
            System.out.println("No Of Null Values of Education: " + dbh.getNoOfNull("dwh.student", "Education"));
            System.out.println("No Of Null Values of Gender: " + dbh.getNoOfNull("dwh.student", "Gender"));
            System.out.println("No Of Null Values of Address: " + dbh.getNoOfNull("dwh.student", "Address"));
            ////////////////////////////////////////
            System.out.println("For Registration:");
            System.out.println("Unique Values of ID: " + dbh.getUniqueValues("dwh.registration", "ID"));
            System.out.println("Unique Values of Degree: " + dbh.getUniqueValues("dwh.registration", "Degree"));
            System.out.println("Unique Values of Discipline: " + dbh.getUniqueValues("dwh.registration", "Discipline"));
            System.out.println("Unique Values of Session: " + dbh.getUniqueValues("dwh.registration", "Session"));
            System.out.println("Unique Values of Marks: " + dbh.getUniqueValues("dwh.registration", "Marks"));
            System.out.println("Unique Values of Course: " + dbh.getUniqueValues("dwh.registration", "Course"));

            //NULL VALUES
            System.out.println("No Of Null Values of ID: " + dbh.getNoOfNull("dwh.registration", "ID"));
            System.out.println("No Of Null Values of Degree: " + dbh.getNoOfNull("dwh.registration", "Degree"));
            System.out.println("No Of Null Values of Course: " + dbh.getNoOfNull("dwh.registration", "Course"));
            System.out.println("No Of Null Values of Marks: " + dbh.getNoOfNull("dwh.registration", "Marks"));
            System.out.println("No Of Null Values of Discipline: " + dbh.getNoOfNull("dwh.registration", "Discipline"));
            System.out.println("No Of Null Values of Session: " + dbh.getNoOfNull("dwh.registration", "Session"));

        ////////////////////////////////////////
            //NO OF COURSES
            System.out.println("Total No Of Courses: " + dbh.getNoOfCourses("dwh.registration", "Course"));
            //No of MALE Students
            System.out.println("Total No of Male Students: " + dbh.getNoOfMale("dwh.student", "Gender"));
            //No of Female Students
            System.out.println("Total No of Female Students: " + dbh.getNoOfFemale("dwh.student", "Gender"));
            //No of UNIQUE Students
            System.out.println("Total No of Unique Students: " + dbh.getUniqueStudents("dwh.student"));
            //No of Average Students per Semester
            System.out.println("No of Average Students per Semester: " + dbh.getAverageStudentsPerSemester("dwh.registration","session","ID"));
            //No of Average Students per Batch
            System.out.println("No of Average Students per Batch: " + dbh.getAverageStudentsPerBatch("dwh.student","Batch","ID"));
        }
    }

    public void dataTransformation() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException, InterruptedException {
            DBHandler dbh = new DBHandler();
            String inputLine;
            BufferedReader br= new BufferedReader(new FileReader(new File("G:\\cities.txt")));
            ArrayList<City> cty = new ArrayList<>();
            while((inputLine = br.readLine()) != null)
            {
                City c = new City(inputLine);
                cty.add(c);
            }
                dbh.populateCity(cty);
       
            dbh.cityStandardization("dwh.student");
            dbh.nameStandardization("dwh.student");
            dbh.transformIslamabad();
        
    }
    
    
}
