/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Islamabad;

import java.util.ArrayList;

/**
 *
 * @author Aqeel's
 */
public class City {
     String city;
    ArrayList<City> clist;
    public City() {
     
    }

    public ArrayList<City> getClist() {
        return clist;
    }

    public void setClist(City c) {
        this.clist.add(c);
    }
    
    public boolean existsAlready(String s)
    {
        
        for(City c:this.clist)
        {
            if(c.getCity().equals(s))
                return true;
            
        }
        return false;
    }
    
    public  City(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    
}
