/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Islamabad;

import java.sql.Date;

/**
 *
 * @author Aqeel's
 */
public class Student {
    String ID;
    String student_fname;
    String student_lname;
    String father_fname;
    String father_lname;
    
    char RegStatus;
    char DegreeStatus;
    String Education;
    String Address;
    
    Date RegDate;
    Date DateOfBirth;
    char Gender;
    int Batch;
    String City;
    
    public Student() {
    }

    public Student(String ID, String student_fname, String student_lname, String father_fname, String father_lname, char RegStatus, char DegreeStatus, String Education, String Address, Date RegDate, Date DateOfBirth, char Gender, int Batch, String City) {
        this.ID = ID;
        this.student_fname = student_fname;
        this.student_lname = student_lname;
        this.father_fname = father_fname;
        this.father_lname = father_lname;
        this.RegStatus = RegStatus;
        this.DegreeStatus = DegreeStatus;
        this.Education = Education;
        this.Address = Address;
        this.RegDate = RegDate;
        this.DateOfBirth = DateOfBirth;
        this.Gender = Gender;
        this.Batch = Batch;
        this.City = City;
    }

    public String getStudent_fname() {
        return student_fname;
    }

    public void setStudent_fname(String student_fname) {
        this.student_fname = student_fname;
    }

    public String getStudent_lname() {
        return student_lname;
    }

    public void setStudent_lname(String student_lname) {
        this.student_lname = student_lname;
    }

    public String getFather_fname() {
        return father_fname;
    }

    public void setFather_fname(String father_fname) {
        this.father_fname = father_fname;
    }

    public String getFather_lname() {
        return father_lname;
    }

    public void setFather_lname(String father_lname) {
        this.father_lname = father_lname;
    }

    
    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public int getBatch() {
        return Batch;
    }

    public void setBatch(int Batch) {
        this.Batch = Batch;
    }

    

    public char getRegStatus() {
        return RegStatus;
    }

    public void setRegStatus(char RegStatus) {
        this.RegStatus = RegStatus;
    }

    public char getDegreeStatus() {
        return DegreeStatus;
    }

    public void setDegreeStatus(char DegreeStatus) {
        this.DegreeStatus = DegreeStatus;
    }

    public char getGender() {
        return Gender;
    }

    public void setGender(char Gender) {
        this.Gender = Gender;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public String getEducation() {
        return Education;
    }

    public void setEducation(String Education) {
        this.Education = Education;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Date getRegDate() {
        return RegDate;
    }

    public void setRegDate(Date RegDate) {
        this.RegDate = RegDate;
    }

    public Date getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(Date DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }
  
    
}
