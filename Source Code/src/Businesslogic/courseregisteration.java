/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;

/**
 *
 * @author sibghat
 */
public class courseregisteration {
private String stid;
private String degree;
private String semester;
private String course;
private String marks;
private String discipline;

public courseregisteration()
{
    
}
    public courseregisteration(String stid, String degree, String semester, String course, String marks, String discipline) {
        this.stid = stid;
        this.degree = degree;
        this.semester = semester;
        this.course = course;
        this.marks = marks;
        this.discipline = discipline;
    }

    public String getStid() {
        return stid;
    }

    public void setStid(String stid) {
        this.stid = stid;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    
}
