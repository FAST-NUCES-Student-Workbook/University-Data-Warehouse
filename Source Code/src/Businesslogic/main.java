/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;

import Transformation.Tranformationtask;
import Transformation.dataProfiling;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;
import net.java.frej.fuzzy.Fuzzy;
import persistentstorage.dbHandler;

/**
 *
 * @author Abubaker
 */
public class main {

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, SQLException, InterruptedException, ParseException {

        String lahoreStudent = "G:\\Semester-7\\DWH\\Project\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject-Dataset\\lhrstudent";
        String peshawarStudent = "G:\\Semester-7\\DWH\\Project\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject-Dataset\\PSH";

        String lahoreReg = "G:\\Semester-7\\DWH\\Project\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject-Dataset\\LHRREG";
        String peshawarReg = "G:\\Semester-7\\DWH\\Project\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject\\DW-Fall2014-ClassProject-Dataset\\pshreg";

        final File folder = new File(lahoreStudent);
        final File folder1 = new File(peshawarStudent);
        final File folder2 = new File(lahoreReg);
        final File folder3 = new File(peshawarReg);
        controller ctr = new controller();
        dbHandler db = new dbHandler();
    //    ctr.listFilesForFolder(folder);
        //    ctr.listFilesForFolder(folder1);
        //    ctr.listFilesForFolder(folder2);
        //    ctr.listFilesForFolder(folder3);
        //  dataProfiling dpf=new dataProfiling();
        Transformation.Tranformationtask tsk = new Tranformationtask();

        tsk.setGender();
        tsk.lookuptable();
        tsk.namingstandardization("dwhlahore.peshawer_student");
        tsk.namingstandardization("dwhlahore.lahore_student");
        tsk.standard_Namepeshawer();
        tsk.standard_Namelahore();
        db.populatecity();
        tsk.standard_citypeshawer();
        tsk.standard_citylahore();
    }
}
