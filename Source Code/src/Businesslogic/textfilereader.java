/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import persistentstorage.dbHandler;


/**
 *
 * @author Abubaker
 */
public class textfilereader {
    
   String filename;
   String campus;
   String batch;
   String degree;
   String pdegree;
   String decider;
   String fileurl;
   File textfile;
   BufferedReader br;
   FileReader fr;
   ArrayList<student> slist=new ArrayList<>();
    public textfilereader(String name) throws FileNotFoundException
    {
        pdegree="BS";
        campus=" ";
        batch=" ";
        degree="BS";
        decider=" ";
        this.fileurl =name;
        this.textfile = new File(fileurl);
        this.fr = new FileReader(textfile);
        this.br = new BufferedReader(fr); 
        filename=textfile.getName();
    }
////////////////////Campus setting ///////////////////////////////
 public void setCampus()
 {
     String []temps=filename.split("_");
     int tokencount = temps.length;
     for(int j=0;j<tokencount;j++)
     {
      if(temps[j].startsWith("Lhr"))
      {
         campus="L";

      } 
      if(temps[j].startsWith("P"))
      {
         campus="P";
      } 
 }}
 ///////////////////////////////////////////set Degree//////////////////////////////
 public void setDegree()
 {
     String []temps=filename.split("_");
     int tokencount = temps.length;
     for(int j=0;j<tokencount;j++)
     {
          if(temps[j].startsWith("BS"))
      {
          degree="BS";
          break;

      }
           if(temps[j].startsWith("MS"))
      {
          degree="MS";
          break;

      }
     }
     
  }
 ////////////////////////////////////Set Batch ////////////////////////////////
 public void setBatch()
 {
        String []temps=filename.split("_");
        int tokencount = temps.length;
        for(int j=0;j<tokencount;j++)
        {
           
           if(temps[j].startsWith("94"))
        {
            batch="94";
            break;
         } 
           if(temps[j].startsWith("95"))
        {
            batch="95";
            break;
         } 
           if(temps[j].startsWith("96"))
        {
            batch="96";
            break;
         } 
           if(temps[j].startsWith("97"))
        {
            batch="97";
            break;
         } 
           if(temps[j].startsWith("98"))
        {
            batch="98";
            break;
         } 
           if(temps[j].startsWith("99"))
        {
            batch="99";
            break;
         } 
           if(temps[j].startsWith("100"))
        {
            batch="00";
            break;
         } 
           if(temps[j].startsWith("101"))
        {
            batch="01";
            break;
         } 
           if(temps[j].startsWith("102"))
        {
            batch="02";
            break;
         } 
           if(temps[j].startsWith("103"))
        {
            batch="03";
            break;
         } 
           if(temps[j].startsWith("104"))
        {
            batch="04";
            break;
         } 
         }
 }
 ////////////////Setting decider/////////////////////////
 public void setDecider()
 {
      String []temps=filename.split("_");
      int tokencount = temps.length;
      for(int j=0;j<tokencount;j++)
      {
       if(temps[j].startsWith("Student"))
      {
         decider="Student";
         

      } 
       if(temps[j].startsWith("Reg"))
      {
         decider="Reg";

      } 
       if(temps[j].startsWith("Detail"))
      {
         decider="Detail";

      } 
 }}
 public void transfercases(String name,String path) throws IOException, ParseException, ClassNotFoundException, SQLException, InterruptedException
 {
    // System.out.println("Transfer cases");
     ArrayList <student> stlist=new ArrayList<>();
        dbHandler db=new dbHandler();
        int count = 0;
        String tempid="0";
        String delims=",";
        String read="read";
        
        //degree="";
        
 ///////////////////Setting campus,degree and batch//////////////////////////////////////////       
        
 //////////////////////////Now reading file and sendind data /////////////////////////////       
   
    while ((read = br.readLine()) != null)
    {
     student stud=new student();
     stud.setGender(' ');
     count++;
     if (count > 1)
     {
        String[] tokens = read.split(delims);
        int tokenCount = tokens.length;
         for (int j = 0; j < tokenCount; j++) {
         tempid = tokens[j];
         if(j==0)
         {
         stud.setSid("L"+"BS"+"-"+tempid);
         
         }
         if (j == 1)
         {
          int nullcounter=0;
         String []array=tokens[j].split(" ");
          for(int i=0;i<array.length;i++)
        {
            if(!array[i].equals(""))
            {
                array[nullcounter]=array[i];
                nullcounter++;
            }
        }
           if(nullcounter<2)
        {
            stud.setFname(array[0]);
            stud.setLname(" ");
            
        }
        if(nullcounter==2)
        {
            stud.setFname(array[0]);
            stud.setLname(array[1]);
            
        }
        if(nullcounter>2)
        {
            
          stud.setFname(array[0]);
          stud.setLname(array[1]+array[2]);
            
       }
        
          }
         if (j == 2)
         {
             String father_fname="NA";
            int count2=0;
            java.util.StringTokenizer tokenizerr1 = new java.util.StringTokenizer(tokens[j], " ");
            StringBuilder flname = new StringBuilder();
            while (tokenizerr1.hasMoreTokens()) {
               
                if(count2==0)
                {
                    father_fname=tokenizerr1.nextToken();
                    count2++;
                }
                else
                {
                    flname.append(tokenizerr1.nextToken()+" ");
                    count2++;
                }
              
            }
            String father_lname = flname.toString();
            
             if (father_fname.isEmpty()) {
                father_fname="NA";
            } 
            if (father_lname.isEmpty()) {
                father_lname="NA";
            } 
            stud.setFathefrname(father_fname);
            stud.setFathelrname(father_lname);
         
        }
          if (j == 3)
          {
            if(tokens[j].equalsIgnoreCase("M"))
            {
                 stud.setGender('M');
            }
            else
            {
                stud.setGender('F');
            }
            
          }
          if(j==4)
          {
              stud.setAddres(tokens[j]);
              ArrayList<String> cty = new ArrayList<>(); 
            java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(tokens[j], " "); 
            while(tokenizerr3.hasMoreTokens())
            {
                cty.add(tokenizerr3.nextToken());
            }
            String City=cty.get(cty.size()-1).toLowerCase();
            stud.setCity(City);
          }
          
            if (j == 5) {
              Calendar mydate = new GregorianCalendar();
             java.util.Date thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(tokens[j]);
             mydate.setTime(thedate);
             int year=mydate.get(Calendar.YEAR)-1900;
             int month=mydate.get(Calendar.MONTH);
             int day=mydate.get(Calendar.DAY_OF_MONTH);
             Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
             stud.setDob(DateOfBirth);
            }
           
           if (j == 6)
           {
            Calendar tdate = new GregorianCalendar();
             java.util.Date thdate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(tokens[j]);
             tdate.setTime(thdate);
             int yr=tdate.get(Calendar.YEAR)-1900;
             int mnth=tdate.get(Calendar.MONTH);
             int dy=tdate.get(Calendar.DAY_OF_MONTH);
             Date regs=new Date(yr,mnth,dy);//rs.getString("Date of Birth");
             stud.setRegdate(regs);
           }
           if (j == 7) {
           stud.setRegstatus(tokens[j].charAt(0));
           }
           if (j == 8)
           {
            stud.setDegreestatus(tokens[j].charAt(0));
           }
           if (j == 9)
           {
          stud.setLastdegree(tokens[j]);
           }
          
        }
         stlist.add(stud);
      //   System.out.println(stud.getSid());
       }
          
         
      }
        db.addlStudent(stlist);  
     
 }
/////////////////////Selects the required operation///////////////////////////
 public void general(String name,String path) throws IOException, ClassNotFoundException, SQLException, InterruptedException, ParseException
 {
    
     if("Transfer.txt".equals(name))
     {
        
         this.transfercases(name, path);
     }
     else
     {
        this.setCampus();
        this.setBatch();
        this.setDegree();
        this.setDecider();
     if("L".equals(campus))
     {
        if(!"Student".equals(decider))
        {
            
            this.lahoreregisteration(name);
        }
       else if("Student".equals(decider))
        {
            this.readlahoreStudentData();
        }
     }
     if("P".equals(campus))
     {
        if("Reg".equals(decider))
        {
            this.peshawerregisteration();
        }
       else if(!"Reg".equals(decider))
        {
            this.readpeshawerstudent();
        } 
     }
  //   System.out.println(campus+degree+batch+decider);
 }}
////////////////////////Reading lahore registeration data //////////////////////////////////////////////////////
public void lahoreregisteration(String name) throws IOException, ClassNotFoundException, SQLException
{
  ArrayList <courseregisteration>rlist=new ArrayList<>();
  dbHandler db=new dbHandler();
  int count = 0;
  String delims=",";
  String read=" ";
   while ((read = br.readLine()) != null)
  {
     courseregisteration crg=new courseregisteration();
     count++;
    
    if(count>1)
    {
        String []tokens=read.split(delims);
        for(int j=0;j<tokens.length;j++)
        {
            if(j==0)
            {
                crg.setStid(campus+degree+"-"+tokens[j]);
            }
            if(j==1)
            {
                crg.setDegree(tokens[j]);
            }
           
            if(j==2)
            {
                if(name.contains("MS"))
                {
                    if(tokens[j].contains("Fall"))
                    {
                     crg.setSemester("Fall"+name.substring(15,17));}
                    else if(tokens[j].contains("Spring")){
                     crg.setSemester("Spring"+name.substring(15,17));}
                    else if(tokens[j].contains("Summer")){
                     crg.setSemester("Summer"+name.substring(15,17));}
                }
                else
                {
                 crg.setSemester(tokens[j]);
                }
            }
            if(j==3)
            {
                crg.setCourse(tokens[j]);
            }
            if(j==4)
            {
                crg.setMarks(tokens[j]);
            }
             if(j==5)
            {
                crg.setDiscipline(tokens[j]);
            }
        }
       rlist.add(crg);
//       System.out.println(crg.getStid());
    }
 }
    db.addlcourse(rlist); 
}
////////////////////////Read peshawer registeration data ///////////////////////////////
public void peshawerregisteration() throws IOException, ClassNotFoundException, SQLException
{
  ArrayList <courseregisteration>rlist=new ArrayList<>();
  dbHandler db=new dbHandler();
  int count = 0;
  String delims=",";
  String read=" ";
   while ((read = br.readLine()) != null)
  {
     courseregisteration crg=new courseregisteration();
     crg.setDegree(degree);
     count++;
     if(count>1)
     {
        String []tokens=read.split(delims);
        for(int j=0;j<tokens.length;j++)
        {
            if(j==0)
            {
                crg.setStid(campus+degree+"-"+tokens[j]);
            }
            if(j==1)
            {
                crg.setCourse(tokens[j]);
            }
           
            if(j==2)
            {
                crg.setMarks(tokens[j]);
            }
            if(j==3)
            {
                crg.setDiscipline(tokens[j]);
            }
//            if(j==4)
//            {
//                crg.setSemester(tokens[j]);
//            }
             if(j==5)
            {
                crg.setSemester(tokens[4]+tokens[j].substring(2));
            }
        }
       rlist.add(crg);
    //   System.out.println(crg.getStid());
    }
 }
   db.addpcourse(rlist); 
}

////////////////////Peshawer student data reading //////////////////////////////////////////////////
public void readpeshawerstudent() throws IOException, ClassNotFoundException, SQLException, InterruptedException, ParseException
{
      ArrayList <student> stlist=new ArrayList<>();
        dbHandler db=new dbHandler();
        int count = 0;
        String tempid="0";
        String delims=",";
        String read="read";
        
        //degree="";
        
 ///////////////////Setting campus,degree and batch//////////////////////////////////////////       
        
 //////////////////////////Now reading file and sendind data /////////////////////////////       
   
    while ((read = br.readLine()) != null)
    {
     student stud=new student();
     stud.setGender(' ');
     count++;
     if (count > 1)
     {
        String[] tokens = read.split(delims);
        int tokenCount = tokens.length;
         for (int j = 0; j < tokenCount; j++) {
         tempid = tokens[j];
         if(j==0)
         {
         stud.setSid(campus+degree+"-"+tempid);
         
         }
         if (j == 1)
         {
          int nullcounter=0;
         String []array=tokens[j].split(" ");
          for(int i=0;i<array.length;i++)
        {
            if(!array[i].equals(""))
            {
                array[nullcounter]=array[i];
                nullcounter++;
            }
        }
           if(nullcounter<2)
        {
            stud.setFname(array[0]);
            stud.setLname(" ");
            
        }
        if(nullcounter==2)
        {
            stud.setFname(array[0]);
            stud.setLname(array[1]);
            
        }
        if(nullcounter>2)
        {
            
          stud.setFname(array[0]);
          stud.setLname(array[1]+array[2]);
            
       }
        
          }
         if (j == 2)
         {
          String[] token = tokens[j].split(" ");
         if(token.length>2)
         {
             if(token.length==3)
             {
                stud.setFathefrname(token[0]+token[1]);
                stud.setFathelrname(token[2]); 
             }
             if(token.length==4)
             {
                stud.setFathefrname(token[0]+token[1]+token[2]);
                stud.setFathelrname(token[3]);
             }
             
         }
         else if(token.length==2)
         {
          stud.setFathefrname(token[0]);
           stud.setFathelrname(token[1]);
         }
         
        }
          if (j == 3)
          {
             stud.setAddres(tokens[j]);
            ArrayList<String> cty = new ArrayList<>(); 
            java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(tokens[j], " "); 
            while(tokenizerr3.hasMoreTokens())
            {
                cty.add(tokenizerr3.nextToken());
            }
            String City=cty.get(cty.size()-1).toLowerCase();
            stud.setCity(City);

          }
            if (j == 4) {
              Calendar mydate = new GregorianCalendar();
             java.util.Date thedate = new SimpleDateFormat("dd-mm-yy", Locale.ENGLISH).parse(tokens[j]);
             mydate.setTime(thedate);
             int year=mydate.get(Calendar.YEAR)-1900;
             int month=mydate.get(Calendar.MONTH);
             int day=mydate.get(Calendar.DAY_OF_MONTH);
             Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
             stud.setDob(DateOfBirth);
            }
           if (j == 5) {
           stud.setLastdegree(tokens[j]);
           }
           if (j == 6)
           {
            Calendar tdate = new GregorianCalendar();
             java.util.Date thdate = new SimpleDateFormat("dd-MM-yy", Locale.ENGLISH).parse(tokens[j]);
             tdate.setTime(thdate);
             int yr=tdate.get(Calendar.YEAR)-1900;
             int mnth=tdate.get(Calendar.MONTH);
             int dy=tdate.get(Calendar.DAY_OF_MONTH);
             Date regs=new Date(yr,mnth,dy);//rs.getString("Date of Birth");
             stud.setRegdate(regs);
           }
           if (j == 7)
           {
             stud.setRegstatus(tokens[j].charAt(0));
           }
           if (j == 8)
           {
           stud.setDegreestatus(tokens[j].charAt(0));
           }
          
        }
         stlist.add(stud);
     //    System.out.println(stud.getSid());
       }
          
         
      }
        db.addpStudent(stlist);  
}
/////////////////////////Reading lahore student data //////////////////////////////////////
  public void readlahoreStudentData() throws IOException, ClassNotFoundException, SQLException, InterruptedException, ParseException {
        ArrayList <student> stlist=new ArrayList<>();
        dbHandler db=new dbHandler();
        int count = 0;
        String tempid=" ";
        String delims=",";
        String read=" ";
       while ((read = br.readLine()) != null)
     {
       student stud=new student();
       count++;
       if (count > 1)
       {
        String[] tokens = read.split(delims);
        int tokenCount = tokens.length;
         for (int j = 0; j < tokenCount; j++) {
         tempid = tokens[j];
         if(j==0)
         {
         stud.setSid(campus+degree+"-"+tempid);
         
         }
         if (j == 1)
         {
             
         int nullcounter=0;
         String []array=tokens[j].split(" ");
          for(int i=0;i<array.length;i++)
        {
            if(!array[i].equals(""))
            {
                array[nullcounter]=array[i];
                nullcounter++;
            }
        }
           if(nullcounter<2)
        {
            stud.setFname(array[0]);
            stud.setLname(" ");
            
        }
        if(nullcounter==2)
        {
            stud.setFname(array[0]);
            stud.setLname(array[1]);
            
        }
        if(nullcounter>2)
        {
            
          stud.setFname(array[0]);
          stud.setLname(array[1]+array[2]);
            
       }
        
         }
         if (j == 2)
         {
            String[] token = tokens[j].split(" ");
            if(token.length<2)
         {
             stud.setFname(token[0]);
             stud.setLname(" ");
         }
            if(token.length>2)
         {
             if(token.length==3)
             {
                stud.setFathefrname(token[0]+token[1]);
                stud.setFathelrname(token[2]); 
             }
             if(token.length==4)
             {
                 stud.setFathefrname(token[0]+token[1]+token[2]);
                 stud.setFathelrname(token[3]);
             }
             
         }
         else if(token.length==2)
         {
           stud.setFathefrname(token[0]);
           stud.setFathelrname(token[1]);
         }
            
        }
          if (j == 3)
          {
            //stud.setGender(tokens[j]);
              if(tokens[j].equals("0"))
              {
              stud.setGender('M');
              }
              if(tokens[j].equals("1"))
              {
                  stud.setGender('F');
              }
              else
              {
                  stud.setGender('M');
              }
            
          }
            if (j == 4) {
             stud.setAddres(tokens[j]);
             ArrayList<String> cty = new ArrayList<>(); 
            java.util.StringTokenizer tokenizerr3 = new java.util.StringTokenizer(tokens[j], " "); 
            while(tokenizerr3.hasMoreTokens())
            {
                cty.add(tokenizerr3.nextToken());
            }
            String City=cty.get(cty.size()-1).toLowerCase();
            stud.setCity(City);
            }
           if (j == 5) {
           
             if(!tokens[j].equals(""))
             {
           //  System.out.println(tokens[j]);
             Calendar mydate = new GregorianCalendar();
             java.util.Date thedate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(tokens[j]);
             mydate.setTime(thedate);
             int year=mydate.get(Calendar.YEAR)-1900;
             int month=mydate.get(Calendar.MONTH);
             int day=mydate.get(Calendar.DAY_OF_MONTH);
             Date DateOfBirth=new Date(year,month,day);//rs.getString("Date of Birth");
             stud.setDob(DateOfBirth);
             }
             else
             {
              // System.out.println("Date");
                Calendar tdate = new GregorianCalendar();
             java.util.Date thdate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(tokens[j+1]);
             tdate.setTime(thdate);
             int yr=tdate.get(Calendar.YEAR)-1900-17;
             int mnth=tdate.get(Calendar.MONTH);
             int dy=tdate.get(Calendar.DAY_OF_MONTH);
             Date regs=new Date(yr,mnth,dy);//rs.getString("Date of Birth");
             stud.setDob(regs); 
                     
             }
           }
           if (j == 6)
           {
             Calendar tdate = new GregorianCalendar();
             java.util.Date thdate = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH).parse(tokens[j]);
             tdate.setTime(thdate);
             int yr=tdate.get(Calendar.YEAR)-1900;
             int mnth=tdate.get(Calendar.MONTH);
             int dy=tdate.get(Calendar.DAY_OF_MONTH);
             Date regs=new Date(yr,mnth,dy);//rs.getString("Date of Birth");
             stud.setRegdate(regs);
           }
           if (j == 7)
           {
             stud.setRegstatus(tokens[j].charAt(0));
           }
           if (j == 8)
           {
           stud.setDegreestatus(tokens[j].charAt(0));
           }
           if (j == 9)
           {
           stud.setLastdegree(tokens[j]);
           } 
        }
         stlist.add(stud);
   //      System.out.println(stud.getSid());
       }
          
        
      }
        db.addlStudent(stlist);
}
}



 