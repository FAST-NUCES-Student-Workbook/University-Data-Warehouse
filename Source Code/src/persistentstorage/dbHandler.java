/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistentstorage;

import Businesslogic.courseregisteration;
import Businesslogic.student;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sibghat
 */
public class dbHandler {
private static dbHandler instance;
private Connection con;
 public dbHandler()
  {
    
    
  }
 public Connection gettConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String host = "jdbc:mysql://localhost:3306/mysql?useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
        String uName = "root";
        String uPass = "admin";
        Connection con = DriverManager.getConnection(host, uName, uPass);
        return con;
    }

   public void populatecity() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException
   {
            Connection conn=this.gettConnection();
         //  conn.setAutoCommit(false); 
            String inputLine;
          BufferedReader br= new BufferedReader(new FileReader(new File("G:\\cities.txt")));
          ArrayList<String> cty = new ArrayList<>();
            while((inputLine = br.readLine()) != null)
            {
                
                
                cty.add(inputLine);
            }
               this.addcity(cty);
   }
   public void addcity(ArrayList<String> stri) throws ClassNotFoundException, SQLException
   {
       Connection conn=this.gettConnection();
     //  conn.setAutoCommit(false);
       String sql="INSERT INTO dwhlahore.city (cityname"+") VALUES(?)"; 
       PreparedStatement stmt =conn.prepareStatement(sql);
       for(String st:stri)
       {
           stmt.setString(1,st);
           stmt.addBatch();
       }
       stmt.executeBatch();
       conn.close();
   }
   public void addlStudent(ArrayList<student> stud) throws ClassNotFoundException, SQLException, InterruptedException
    {
        
        Connection conn=this.gettConnection();
       // conn.setAutoCommit(false); 
        String sql1 = "INSERT INTO dwhlahore.lahore_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";    
        PreparedStatement stmt =conn.prepareStatement(sql1);
        final int batchSize = 1000;
        int count = 0;
        for(student stu:stud)
        {
            stmt.setString(1, stu.getSid());
            
            if(!stu.getFname().equals(" "))
            {
                stmt.setString(2,stu.getFname());
            }
            else
            {
                stmt.setString(2,"NA");
            }
            if(!stu.getLname().equals(" "))
            {
                stmt.setString(3,stu.getLname());
            }
            else
            {
                stmt.setString(3,"NA");
            }
            
            if(stu.getFatherfname()!=null)
            {
                stmt.setString(4,stu.getFatherfname());
            }
            else
            {
                stmt.setString(4,"NA");
            }
            if(stu.getFatherlname()!=null)
            {
                stmt.setString(5,stu.getFatherlname());
            }
            else
            {
                stmt.setString(5,"NA");
            }
            
            
            
            stmt.setString(6,Character.toString(stu.getGender()));
            
            stmt.setDate(7, stu.getDob());
            stmt.setDate(8, stu.getRegdate());
            stmt.setString(9,Character.toString(stu.getRegstatus()));
            stmt.setString(10,Character.toString(stu.getDegreestatus()));
            stmt.setString(11, stu.getLastdegree());
             stmt.setString(12, stu.getCity());
             stmt.setString(13, stu.getAddres());
         //    System.out.println(stu.getSid());
            stmt.addBatch();
           
            }
       stmt.executeBatch();
       
        conn.close();
  
    }
   public void addpStudent(ArrayList<student> stud) throws ClassNotFoundException, SQLException, InterruptedException
    {
        Connection conn=this.gettConnection();
      //  conn.setAutoCommit(false); 
        String sql1 = "INSERT INTO dwhlahore.peshawer_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(sql1);
        final int batchSize = 1000;
        int count = 0;
        for(student stu:stud)
        {
            stmt.setString(1, stu.getSid());
            
             if(!stu.getFname().equals(" "))
            {
                stmt.setString(2,stu.getFname());
            }
            else
            {
                stmt.setString(2,"NA");
            }
            if(!stu.getLname().equals(" "))
            {
                stmt.setString(3,stu.getLname());
            }
            else
            {
                stmt.setString(3,"NA");
            }
            
            if(stu.getFatherfname()!=null)
            {
                stmt.setString(4,stu.getFatherfname());
            }
            else
            {
                stmt.setString(4,"NA");
            }
            if(stu.getFatherlname()!=null)
            {
                stmt.setString(5,stu.getFatherlname());
            }
            else
            {
                stmt.setString(5,"NA");
            }
            
            
            stmt.setString(6,Character.toString(stu.getGender()));
            
            stmt.setDate(7, stu.getDob());
            stmt.setDate(8, stu.getRegdate());
            stmt.setString(9,Character.toString(stu.getRegstatus()));
            stmt.setString(10,Character.toString(stu.getDegreestatus()));
            stmt.setString(11, stu.getLastdegree());
            stmt.setString(12, stu.getCity());
            stmt.setString(13, stu.getAddres());
            stmt.addBatch();
           
            }
       stmt.executeBatch();
        conn.close();
    }
    public void addlcourse(ArrayList<courseregisteration> cr) throws ClassNotFoundException, SQLException
    {
        
        Connection conn=this.gettConnection();
        
        //conn.setAutoCommit(false); 
        String sql1 = "INSERT INTO dwhlahore.lahore_registeration(student_id,degree,semester,course_code,marks,discipline,grade"+") VALUES (?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(sql1);
        final int batchSize = 1000;
        int count = 0;
        for(courseregisteration cru:cr)
        {
            
            stmt.setString(1, cru.getStid());
            stmt.setString(2, cru.getDegree());
            stmt.setString(3, cru.getSemester());
            stmt.setString(4,cru.getCourse());
            stmt.setInt(5,Integer.parseInt(cru.getMarks()));
            stmt.setString(6,cru.getDiscipline());
            stmt.setString(7, this.grade_calculate(Integer.parseInt(cru.getMarks())));
           // System.out.println(cru.getStid());
            stmt.addBatch();
             if (++count % batchSize == 0) {
                stmt.executeBatch();
            }
           
            }
         stmt.executeBatch();
        conn.close();
}
      public String grade_calculate(int marks) throws ClassNotFoundException, SQLException
{
       String grade="C";
       
       if(marks>80)
       {
          grade="A+";
       }
       if(marks<80 && marks>75)
       {
          grade="A";
       }
       if(marks<75 && marks>70)
       {
           grade="A-";
       }
       if(marks<70 && marks>67)
       {
          grade="B+";
       }
       if(marks<67 && marks>60)
       {
           grade="B";
       }
       if(marks<60 && marks>55)
       {
          grade="B-";
       }
        if(marks<55 && marks>50)
       {
          grade="C+";
       }
         if(marks<55 && marks>53)
       {
          grade="C";
       }
         if(marks<53 && marks>50)
       {
          grade="C-";
       }
         if(marks<45 && marks>40)
       {
           grade="D+";
       }
         if(marks<40 && marks>38)
       {
           grade="D";
       }
       
        if(marks<38)
       {
           grade="F";
       }
       return grade;
    }

     public void addpcourse(ArrayList<courseregisteration> cr) throws ClassNotFoundException, SQLException
    {
        Connection conn=this.gettConnection();
       // conn.setAutoCommit(false); 
        String sql1 = "INSERT INTO dwhlahore.peshawer_registeration(student_id,degree,semester,course_code,marks,discipline,grade"+") VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt =conn.prepareStatement(sql1);
        final int batchSize = 1000;
        int count = 0;
        for(courseregisteration cru:cr)
        {
            stmt.setString(1, cru.getStid());
            stmt.setString(2, cru.getDegree());
            stmt.setString(3, cru.getSemester());
            stmt.setString(4,cru.getCourse());
            stmt.setInt(5,Integer.parseInt(cru.getMarks()));
            stmt.setString(6,cru.getDiscipline());
             stmt.setString(7, this.grade_calculate(Integer.parseInt(cru.getMarks())));
            stmt.addBatch();
           
            }
       stmt.executeBatch();
        conn.close();
}
}
