/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Transformation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import net.java.frej.fuzzy.Fuzzy;
import persistentstorage.dbHandler;

/**
 *
 * @author sibghat
 */
public class Tranformationtask {

 
 public Connection gettConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String host = "jdbc:mysql://localhost:3306/mysql?useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
        String uName = "root";
        String uPass = "admin";
        Connection con = DriverManager.getConnection(host, uName, uPass);
        return con;
    }
public void createlookuptable() throws ClassNotFoundException, SQLException
{
    
    Connection conn=this.gettConnection();
   // conn.setAutoCommit(false); 
    String sql1 = "INSERT INTO dwhlahore.gender_lookup(name,gender,count"+") VALUES (?,?,?)";   
    PreparedStatement stmt =conn.prepareStatement(sql1);
    String sql="select student_fname,gender,count(*) from dwhlahore.lahore_student group by student_fname,gender";
    Statement stm =conn.createStatement();
    ResultSet rs=stm.executeQuery(sql);
    final int batchSize = 1000;
    int count = 0;
    while(rs.next())
    {
        stmt.setString(1,rs.getString(1));
        stmt.setString(2,Character.toString(rs.getString(2).charAt(0)));
        stmt.setInt(3,rs.getInt(3));
        stmt.addBatch();
    }
    stmt.executeBatch();
    conn.close();
}
public void lookuptable() throws ClassNotFoundException, SQLException
{
    
    Connection conn=this.gettConnection();
 //   conn.setAutoCommit(false); 
    String sql1 = "INSERT INTO dwhlahore.gender_lookup(name,gender,count"+") VALUES (?,?,?)";   
    PreparedStatement stmt =conn.prepareStatement(sql1);
    String sql="select student_fname,gender,count(*) from dwhlahore.genderpeshawer_student group by student_fname,gender";
    Statement stm =conn.createStatement();
    ResultSet rs=stm.executeQuery(sql);
    final int batchSize = 1000;
    int count = 0;
    while(rs.next())
    {
        stmt.setString(1,rs.getString(1));
        stmt.setString(2,Character.toString(rs.getString(2).charAt(0)));
        stmt.setInt(3,rs.getInt(3));
        stmt.addBatch();
    }
    stmt.executeBatch();
    conn.close();
}
   public void setGender() throws ClassNotFoundException, SQLException
   {
       this.createlookuptable();
       int mcount=0;
       int fcount=0;
       Connection conn=this.gettConnection();
   //    conn.setAutoCommit(false); 
       String sql1="SELECT * FROM dwhlahore.peshawer_student";
       Statement stm =conn.createStatement();
       ResultSet pdata=stm.executeQuery(sql1);
        String batch = "INSERT INTO dwhlahore.genderpeshawer_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(batch);
        final int batchSize = 1000;
        int count = 0;
       while(pdata.next())
       {
         
         
         String sql="select * from dwhlahore.gender_lookup WHERE name ='"+pdata.getString(2)+"' and gender='M'";
         Statement stm1 =conn.createStatement();
         ResultSet malecount=stm1.executeQuery(sql);
         String sql2="select * from dwhlahore.gender_lookup WHERE name ='"+pdata.getString(2)+"' and gender='F'";
         Statement stm2 =conn.createStatement();
         ResultSet fmalecount=stm2.executeQuery(sql2);
         if(malecount.next())
         {

        if( !String.valueOf(malecount.getInt(3)).equals(Character.toString(' ')))
        {
            mcount++;
        }

        } 
         if(fmalecount.next())
         {
             if(!String.valueOf(fmalecount.getInt(3)).equals(Character.toString(' ')))
         {

         fcount++;
         
         }
         }
         if(mcount>fcount)
         {
            stmt.setString(1,pdata.getString(1));
            stmt.setString(2,pdata.getString(2));
            stmt.setString(3,pdata.getString(3));
            stmt.setString(4,pdata.getString(4));
            stmt.setString(5,pdata.getString(5));
            stmt.setString(6,Character.toString('M'));
            
            stmt.setDate(7,pdata.getDate(7));
            stmt.setDate(8,pdata.getDate(8));
            stmt.setString(9,Character.toString(pdata.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(pdata.getString(10).charAt(0)));
            stmt.setString(11,pdata.getString(11));
            stmt.setString(12,pdata.getString(12));
            stmt.setString(13,pdata.getString(13));
            stmt.addBatch();
             
         }
         else
         {
             stmt.setString(1,pdata.getString(1));
            stmt.setString(2,pdata.getString(2));
            stmt.setString(3,pdata.getString(3));
            stmt.setString(4,pdata.getString(4));
            stmt.setString(5,pdata.getString(5));
            stmt.setString(6,Character.toString('F'));
            stmt.setDate(7,pdata.getDate(7));
            stmt.setDate(8,pdata.getDate(8));
            stmt.setString(9,Character.toString(pdata.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(pdata.getString(10).charAt(0)));
            stmt.setString(11,pdata.getString(11));
            stmt.setString(12,pdata.getString(12));
            stmt.setString(13,pdata.getString(13));
            stmt.addBatch();
         }
       }
          stmt.executeBatch();
       
        conn.close();
   }
   public void standard_Namepeshawer() throws ClassNotFoundException, SQLException
   {
        
        Connection conn=this.gettConnection();
     //  conn.setAutoCommit(false); 
       String sql1="SELECT * FROM dwhlahore.peshawer_student";
       Statement stm =conn.createStatement();
       ResultSet data=stm.executeQuery(sql1);
       String batch = "INSERT INTO dwhlahore.namingpeshawer_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(batch);
        final int batchSize = 1000;
        int count = 0;
       while(data.next())
      {
            String mysql="select variation,standard_name from dwhlahore.name_lookup where variation='"+data.getString(2)+"'";
            Statement stm1 =conn.createStatement();
            ResultSet ins=stm1.executeQuery(mysql);
            if(ins.next())
            {
            stmt.setString(1,data.getString(1));
            stmt.setString(2,data.getString(2));
            stmt.setString(3,data.getString(3));
            stmt.setString(4,data.getString(4));
            stmt.setString(5,data.getString(5));
            stmt.setString(6,Character.toString('M'));
            
            stmt.setDate(7,data.getDate(7));
            stmt.setDate(8,data.getDate(8));
            stmt.setString(9,Character.toString(data.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(data.getString(10).charAt(0)));
            stmt.setString(11,data.getString(11));
            stmt.setString(12,data.getString(12));
            stmt.setString(13,data.getString(13));
            stmt.addBatch();
          //    System.out.println(" NAME "+data.getString(2)+" Variation "+ins.getString(1));
                
            }
            }
           stmt.executeBatch();
       
        conn.close();
   }
   public void standard_Namelahore() throws ClassNotFoundException, SQLException
   {
    
        Connection conn=this.gettConnection();
   //    conn.setAutoCommit(false); 
       String sql1="SELECT * FROM dwhlahore.lahore_student";
       Statement stm =conn.createStatement();
       ResultSet data=stm.executeQuery(sql1);
       String batch = "INSERT INTO dwhlahore.naminglahore_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(batch);
        final int batchSize = 1000;
        int count = 0;
       while(data.next())
      {
            String mysql="select variation,standard_name from dwhlahore.name_lookup where variation='"+data.getString(2)+"'";
            Statement stm1 =conn.createStatement();
            ResultSet ins=stm1.executeQuery(mysql);
            if(ins.next())
            {
            stmt.setString(1,data.getString(1));
            stmt.setString(2,ins.getString(2));
            stmt.setString(3,data.getString(3));
            stmt.setString(4,data.getString(4));
            stmt.setString(5,data.getString(5));
            stmt.setString(6,Character.toString(data.getString(6).charAt(0)));
            
            
            stmt.setDate(7,date_validation(data.getDate(7),data.getDate(8)));
            stmt.setDate(8,data.getDate(8));
            stmt.setString(9,Character.toString(data.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(data.getString(10).charAt(0)));
            stmt.setString(11,data.getString(11));
            stmt.setString(12,data.getString(12));
            stmt.setString(13,data.getString(13));
            stmt.addBatch();
          //    System.out.println(" NAME "+data.getString(2)+" Variation "+ins.getString(1));
                
            }
            }
           stmt.executeBatch();
       
        conn.close();
   }
   public void standard_citylahore() throws ClassNotFoundException, SQLException
   {
        Connection conn=this.gettConnection();
   //    conn.setAutoCommit(false); 
       String sql1="SELECT * FROM dwhlahore.naminglahore_student";
       Statement stm =conn.createStatement();
       ResultSet data=stm.executeQuery(sql1);
       String batch = "INSERT INTO dwhlahore.citylahore_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(batch);
        final int batchSize = 1000;
        int count = 0;
        while(data.next())
      {
       //  System.out.println(data.getString(12));
            String mysql="select standard_city from dwhlahore.city_lookup where variation='"+data.getString(12)+"'";
            Statement stm1 =conn.createStatement();
            ResultSet ins=stm1.executeQuery(mysql);
            String City=data.getString(12);
            ins.next();
            if(ins.getRow()!=0)
            {
                City=ins.getString(1);
            }
            
            // System.out.println(ins.getString(1));
            stmt.setString(1,data.getString(1));
            stmt.setString(2,data.getString(2));
            stmt.setString(3,data.getString(3));
            stmt.setString(4,data.getString(4));
            stmt.setString(5,data.getString(5));
            stmt.setString(6,Character.toString(data.getString(6).charAt(0)));
            
            
            stmt.setDate(7,date_validation(data.getDate(7),data.getDate(8)));
            stmt.setDate(8,data.getDate(8));
            stmt.setString(9,Character.toString(data.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(data.getString(10).charAt(0)));
            stmt.setString(11,data.getString(11));
            stmt.setString(12,City);
            stmt.setString(13,data.getString(13));
            stmt.addBatch();
          //    System.out.println(" NAME "+data.getString(2)+" Variation "+ins.getString(1));
                
            
            }
           stmt.executeBatch();
       
        conn.close();
   } 
   public void standard_citypeshawer() throws ClassNotFoundException, SQLException, IOException
   {
        
           this.cityStandardization("dwhlahore.naminglahore_student");
         this.cityStandardization("dwhlahore.namingpeshawer_student");
        
        Connection conn=this.gettConnection();
  //     conn.setAutoCommit(false); 
       String sql1="SELECT * FROM dwhlahore.namingpeshawer_student";
       Statement stm =conn.createStatement();
       ResultSet data=stm.executeQuery(sql1);
       String batch = "INSERT INTO dwhlahore.citypeshawer_student(student_id,student_fname,student_lname,father_fname,father_lname,gender,birth_date,reg_date, reg_status,degree_status,last_degree,city,address"+") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";   
        PreparedStatement stmt =conn.prepareStatement(batch);
        final int batchSize = 1000;
        int count = 0;
        while(data.next())
      {
       //  System.out.println(data.getString(12));
            String mysql="select standard_city from dwhlahore.city_lookup where variation='"+data.getString(12)+"'";
            Statement stm1 =conn.createStatement();
            ResultSet ins=stm1.executeQuery(mysql);
            String City=data.getString(12);
            ins.next();
            if(ins.getRow()!=0)
            {
                City=ins.getString(1);
            }
            
            // System.out.println(ins.getString(1));
            stmt.setString(1,data.getString(1));
            stmt.setString(2,data.getString(2));
            stmt.setString(3,data.getString(3));
            stmt.setString(4,data.getString(4));
            stmt.setString(5,data.getString(5));
            stmt.setString(6,Character.toString(data.getString(6).charAt(0)));
            
            
            stmt.setDate(7,date_validation(data.getDate(7),data.getDate(8)));
            stmt.setDate(8,data.getDate(8));
            stmt.setString(9,Character.toString(data.getString(9).charAt(0)));
            stmt.setString(10,Character.toString(data.getString(10).charAt(0)));
            stmt.setString(11,data.getString(11));
            stmt.setString(12,City);
            stmt.setString(13,data.getString(13));
            stmt.addBatch();
          //    System.out.println(" NAME "+data.getString(2)+" Variation "+ins.getString(1));
                
            
            }
           stmt.executeBatch();
       
        conn.close();
   } 
   public ArrayList<String> make_list(String table_name) throws ClassNotFoundException, SQLException
{
    ArrayList<String> list=new ArrayList<>();
    Connection conn=this.gettConnection();
 //   conn.setAutoCommit(false); 
    String sql="select DISTINCT(name) from "+table_name+"";
    Statement stm =conn.createStatement();
    ResultSet rs=stm.executeQuery(sql);
    while(rs.next())
    {
        list.add(rs.getString(1));
    }
    return list;
    
}
   public void namingstandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       //this.lookuptable();
       ArrayList <String> lis=new ArrayList<>();
       lis=this.make_list("dwhlahore.gender_lookup");
       Connection conn=this.gettConnection();
    //   conn.setAutoCommit(false); 
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwhlahore.name_lookup(standard_name,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(2),lis.get(i)))
           {
               stmt.setString(1, rs.getString(2));
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
        
           }    
          }
          
      }
        stmt.executeBatch();
        conn.close();
     //    this.standard_Name();
   }
   public Date date_validation(Date dob,Date regdate)
   {
     
      
      if (regdate.getYear() <= dob.getYear())
      {
          
        dob.setYear(regdate.getYear() - 18);
      
       }
      return dob;
   }
   public void cityStandardization(String table_name) throws ClassNotFoundException, SQLException
   {
       ArrayList <String> lis=new ArrayList<>();
       lis=this.getCities();
       Connection conn=this.gettConnection();
       
       String sql="select * from "+table_name+"";
       Statement stm =conn.createStatement();
       ResultSet rs=stm.executeQuery(sql);
       String batch = "INSERT INTO dwhlahore.city_lookup(standard_city,variation"+") VALUES (?,?)";   
       PreparedStatement stmt =conn.prepareStatement(batch);
       while(rs.next())
       {
           
          for(int i=0;i<lis.size();i++)
          {
          if(Fuzzy.equals(rs.getString(12),lis.get(i)))
           {
               stmt.setString(1, rs.getString(12).toLowerCase());
               stmt.setString(2,lis.get(i));
               stmt.addBatch();
               lis.remove(i);
        
           }    
          }
          
      }
        stmt.setString(1, "islamabad");
        stmt.setString(2, "isb");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rwp");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "raw");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpind");
        stmt.addBatch();
        stmt.setString(1, "rawalpindi");
        stmt.setString(2, "rawalpin");
        stmt.addBatch();
        stmt.setString(1, "abbotabad");
        stmt.setString(2, "abbottabad");
        stmt.addBatch();
        stmt.setString(1, "karachi");
        stmt.setString(2, "khi");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "dg Khan");
        stmt.addBatch();
        stmt.setString(1, "Dera Ghazi Khan");
        stmt.setString(2, "d.g. khan");
        stmt.addBatch();
        stmt.executeBatch();
       
   }
   public ArrayList<String> getCities() throws ClassNotFoundException, SQLException
    {
         Connection con1 = this.gettConnection();
        // Connection conn = this.gettConnection();
        String sql1 = "SELECT DISTINCT(cityname) From dwhlahore.city";
        Statement stmt = con1.createStatement();
        ResultSet rs = stmt.executeQuery(sql1);
        ArrayList<String> city = new ArrayList<>();
        while(rs.next())
        {
            city.add(rs.getString(1).toLowerCase());
        }
        return city;
    }
}
