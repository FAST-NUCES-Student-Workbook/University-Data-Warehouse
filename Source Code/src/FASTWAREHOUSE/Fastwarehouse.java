/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FASTWAREHOUSE;

//import Karachi.DBHandler;
import java.sql.SQLException;

/**
 *
 * @author Abubaker
 */
public class Fastwarehouse {
    
    
     public static void main(String[] args) throws ClassNotFoundException, SQLException 
     {
  
/////////////     Populating Tables by Extracting data from source files/systems  //////////////  
         
        Karachi.DBHandler db =new Karachi.DBHandler();
        System.out.println("Islamabad");
        db.pupulateUniversityStudents("dwh.students");
        System.out.println("Karachi");
        db.pupulateUniversityStudents("dwhkarachi.students");
        System.out.println("Lahore");
        db.pupulateUniversityStudents("dwhlahore.citylahore_student");
        System.out.println("Peshawar");
        db.pupulateUniversityStudents("dwhlahore.citypeshawer_student");
        
        System.out.println("Islamabad Registrations");
        db.pupulateUniversityRegistration("dwh.course_registrations");
        System.out.println("Karachi Registrations");
        db.pupulateUniversityRegistration("dwhkarachi.course_registrations");
        System.out.println("Lahore Registrations");
        db.pupulateUniversityRegistration("dwhlahore.lahore_registeration");
        System.out.println("Peshawar Registrations");
        db.pupulateUniversityRegistration("dwhlahore.peshawer_registeration");


/////////////     Populating Dimension Model  //////////////  
        db.populateDimentionStudent();
        db.populateDimentionCourses();
        db.populateDimentionCampus();
        db.populateDimentionDegree();
        db.populateDimentionsession();
        System.out.println("Populating FACT TABLE");
        db.populateDimentionFAST();
        


        db.calculate_sgpa();   // calculatig CGPA of students
        db.calculate_cgpa();    // calculating SGPA of studnets
        db.topPositionHolders();   // declaring top positions
        
/////////////     Populating Business Questions  //////////////  

            db.cgpa_report();    
            db.businessQ1CommonAddress();
            db.businessQ2AttractedCourses();
            db.businessQ3BackgroundRatio();
            db.businessQ4AvgAgeCampusWise();
            db.businessQ5NameMostUsed();
            db.businessQ6TransfersYearWise();
            db.businessQ7MultipleDegrees();
        
     }
     
     
    
}
