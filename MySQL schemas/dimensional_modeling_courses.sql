CREATE DATABASE  IF NOT EXISTS `dimensional_modeling` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dimensional_modeling`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dimensional_modeling
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_code` varchar(20) NOT NULL,
  `course_type` varchar(45) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'CS-102','CS'),(2,'CS-103','CS'),(3,'CS-104','CS'),(4,'SS-201','CS'),(5,'MS-301','CS'),(6,'CS-101','CS'),(7,'CS-101','TC'),(8,'CS-102','TC'),(9,'CS-103','TC'),(10,'CS-104','TC'),(11,'SS-201','TC'),(12,'MS-301','TC'),(13,'CS-101','SE'),(14,'CS-102','SE'),(15,'CS-103','SE'),(16,'CS-104','SE'),(17,'SS-201','SE'),(18,'MS-301','SE'),(19,'CS-101','CE'),(20,'CS-102','CE'),(21,'CS-103','CE'),(22,'CS-104','CE'),(23,'SS-201','CE'),(24,'MS-301','CE'),(25,'UR-401','CS'),(26,'CS-105','CS'),(27,'CS-106','CS'),(28,'CS-107','CS'),(29,'CS-108','CS'),(30,'CS-109','CS'),(31,'UR-401','TC'),(32,'CS-105','TC'),(33,'CS-106','TC'),(34,'CS-107','TC'),(35,'CS-108','TC'),(36,'CS-109','TC'),(37,'UR-401','SE'),(38,'CS-105','SE'),(39,'CS-106','SE'),(40,'CS-107','SE'),(41,'CS-108','SE'),(42,'CS-109','SE'),(43,'UR-401','CE'),(44,'CS-105','CE'),(45,'CS-106','CE'),(46,'CS-107','CE'),(47,'CS-108','CE'),(48,'CS-109','CE'),(49,'CS-110','CS'),(50,'CS-111','CS'),(51,'SS-202','CS'),(52,'MS-302','CS'),(53,'UR-402','CS'),(54,'CS-112','CS'),(55,'CS-110','TC'),(56,'CS-111','TC'),(57,'SS-202','TC'),(58,'MS-302','TC'),(59,'UR-402','TC'),(60,'CS-112','TC'),(61,'CS-110','SE'),(62,'CS-111','SE'),(63,'SS-202','SE'),(64,'MS-302','SE'),(65,'UR-402','SE'),(66,'CS-112','SE'),(67,'CS-110','CE'),(68,'CS-111','CE'),(69,'SS-202','CE'),(70,'MS-302','CE'),(71,'UR-402','CE'),(72,'CS-112','CE'),(73,'CS-113','CS'),(74,'CS-114','CS'),(75,'CS-115','CS'),(76,'CS-116','CS'),(77,'CS-117','CS'),(78,'CS-118','CS'),(79,'CS-113','TC'),(80,'CS-114','TC'),(81,'CS-115','TC'),(82,'TC-501','TC'),(83,'TC-502','TC'),(84,'TC-503','TC'),(85,'CS-113','SE'),(86,'CS-114','SE'),(87,'CS-115','SE'),(88,'EE-116','SE'),(89,'EE-117','SE'),(90,'CS-118','SE'),(91,'CS-113','CE'),(92,'CS-114','CE'),(93,'CS-115','CE'),(94,'CE-116','CE'),(95,'CE-117','CE'),(96,'CE-118','CE'),(97,'SS-203','CS'),(98,'MS-303','CS'),(99,'UR-403','CS'),(100,'SS-105','CS'),(101,'CS-119','CS'),(102,'CS-120','CS'),(103,'SS-203','TC'),(104,'MS-303','TC'),(105,'UR-403','TC'),(106,'TC-504','TC'),(107,'TC-505','TC'),(108,'TC-506','TC'),(109,'SS-203','SE'),(110,'MS-303','SE'),(111,'UR-403','SE'),(112,'SS-105','SE'),(113,'CS-119','SE'),(114,'CS-120','SE'),(115,'SS-203','CE'),(116,'MS-303','CE'),(117,'UR-403','CE'),(118,'CE-105','CE'),(119,'EE-119','CE'),(120,'EE-120','CE'),(121,'CS-121','CS'),(122,'CS-122','CS'),(123,'CS-123','CS'),(124,'CS-124','CS'),(125,'SS-204','CS'),(126,'MS-304','CS'),(127,'TC-507','TC'),(128,'TC-508','TC'),(129,'TC-509','TC'),(130,'TC-510','TC'),(131,'TC-204','TC'),(132,'MS-304','TC'),(133,'SE-121','SE'),(134,'SE-122','SE'),(135,'SE-123','SE'),(136,'SE-124','SE'),(137,'SS-204','SE'),(138,'MS-304','SE'),(139,'EE-121','CE'),(140,'CS-122','CE'),(141,'CS-123','CE'),(142,'CS-124','CE'),(143,'SS-204','CE'),(144,'MS-304','CE'),(145,'UR-404','CS'),(146,'CS-125','CS'),(147,'CS-126','CS'),(148,'CS-127','CS'),(149,'CS-128','CS'),(150,'CS-129','CS'),(151,'UR-404','TC'),(152,'TC-511','TC'),(153,'TC-512','TC'),(154,'TC-513','TC'),(155,'TC-514','TC'),(156,'TC-515','TC'),(157,'UR-404','SE'),(158,'SE-125','SE'),(159,'CS-126','SE'),(160,'SE-127','SE'),(161,'SE-128','SE'),(162,'SE-129','SE'),(163,'UR-404','CE'),(164,'CS-125','CE'),(165,'CE-126','CE'),(166,'CS-127','CE'),(167,'CS-128','CE'),(168,'EE-129','CE'),(169,'CS-130','CS'),(170,'SS-205','CS'),(171,'MS-305','CS'),(172,'UR-405','CS'),(173,'CS-131','CS'),(174,'CS-132','CS'),(175,'TS-516','TC'),(176,'SS-205','TC'),(177,'MS-305','TC'),(178,'UR-405','TC'),(179,'TC-517','TC'),(180,'TC-518','TC'),(181,'EE-130','SE'),(182,'SS-205','SE'),(183,'MS-305','SE'),(184,'UR-405','SE'),(185,'SE-131','SE'),(186,'EE-132','SE'),(187,'CS-130','CE'),(188,'SS-205','CE'),(189,'MS-305','CE'),(190,'UR-405','CE'),(191,'CS-131','CE'),(192,'CS-132','CE'),(193,'CS-801','MS-CS'),(194,'CS-802','MS-CS'),(195,'CS-703','MS-CS'),(196,'CS-801','MS-SPM'),(197,'CS-802','MS-SPM'),(198,'CS-703','MS-SPM'),(199,'CS-704','MS-CS'),(200,'CS-201','MS-CS'),(201,'CS-301','MS-CS'),(202,'CS-707','MS-SPM'),(203,'CS-204','MS-SPM'),(204,'CS-306','MS-SPM'),(205,'CS-401','MS-CS'),(206,'CS-505','MS-CS'),(207,'CS-606','MS-CS'),(208,'CS-409','MS-SPM'),(209,'CS-501','MS-SPM'),(210,'CS-602','MS-SPM'),(211,'CS-907','MS-CS'),(212,'CS-708','MS-CS'),(213,'CS-968','MS-CS'),(214,'CS-907','MS-SPM'),(215,'CS-708','MS-SPM'),(216,'CS-968','MS-SPM'),(217,'CS-801','MS-TC'),(218,'CS-802','MS-TC'),(219,'CS-703','MS-TC'),(220,'CS-801','MS-NW'),(221,'CS-802','MS-NW'),(222,'CS-703','MS-NW'),(223,'CS-707','MS-TC'),(224,'CS-904','MS-TC'),(225,'CS-317','MS-TC'),(226,'CS-707','MS-NW'),(227,'CS-404','MS-NW'),(228,'CS-307','MS-NW'),(229,'CS-418','MS-TC'),(230,'CS-511','MS-TC'),(231,'CS-612','MS-TC'),(232,'CS-408','MS-NW'),(233,'CS-510','MS-NW'),(234,'CS-622','MS-NW'),(235,'CS-977','MS-TC'),(236,'CS-798','MS-TC'),(237,'CS-968','MS-TC'),(238,'CS-987','MS-NW'),(239,'CS-798','MS-NW'),(240,'CS-968','MS-NW');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-26  9:42:29
