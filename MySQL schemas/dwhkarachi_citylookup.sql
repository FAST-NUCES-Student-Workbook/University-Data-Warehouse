CREATE DATABASE  IF NOT EXISTS `dwhkarachi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dwhkarachi`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dwhkarachi
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `citylookup`
--

DROP TABLE IF EXISTS `citylookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citylookup` (
  `standard_city` varchar(50) NOT NULL,
  `variation` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citylookup`
--

LOCK TABLES `citylookup` WRITE;
/*!40000 ALTER TABLE `citylookup` DISABLE KEYS */;
INSERT INTO `citylookup` VALUES ('thata','thatta'),('quetta','quetta'),('karachi','karachi'),('lyyah','layyah'),('wahari','vehari'),('chishtian','chishtian'),('sadiqabad','hafizabad'),('sadiqabad','sadiqabad'),('sadiqabad','wazirabad'),('sadiqabad','nazimabad'),('hyderabad','hyderabad'),('multan','multan'),('sibbi','sibbi'),('sibbi','sibi'),('islamabad','isb'),('rawalpindi','rwp'),('rawalpindi','raw'),('rawalpindi','rawalpind'),('rawalpindi','rawalpin'),('abbotabad','abbottabad'),('karachi','khr'),('Dera Ghazi Khan','dg Khan'),('Dera Ghazi Khan','d.g. khan'),('thata','thatta'),('quetta','quetta'),('karachi','karachi'),('lyyah','layyah'),('wahari','vehari'),('chishtian','chishtian'),('sadiqabad','hafizabad'),('sadiqabad','sadiqabad'),('sadiqabad','wazirabad'),('sadiqabad','nazimabad'),('hyderabad','hyderabad'),('multan','multan'),('sibbi','sibbi'),('sibbi','sibi'),('islamabad','isb'),('rawalpindi','rwp'),('rawalpindi','raw'),('rawalpindi','rawalpind'),('rawalpindi','rawalpin'),('abbotabad','abbottabad'),('karachi','khr'),('Dera Ghazi Khan','dg Khan'),('Dera Ghazi Khan','d.g. khan');
/*!40000 ALTER TABLE `citylookup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-26  9:42:24
