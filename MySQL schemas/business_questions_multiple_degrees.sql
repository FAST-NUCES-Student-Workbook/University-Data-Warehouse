CREATE DATABASE  IF NOT EXISTS `business_questions` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `business_questions`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: business_questions
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `multiple_degrees`
--

DROP TABLE IF EXISTS `multiple_degrees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multiple_degrees` (
  `Student_id` varchar(10) NOT NULL,
  `Student_Other_degree_id` varchar(10) NOT NULL,
  `Student_Name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multiple_degrees`
--

LOCK TABLES `multiple_degrees` WRITE;
/*!40000 ALTER TABLE `multiple_degrees` DISABLE KEYS */;
INSERT INTO `multiple_degrees` VALUES ('IBS-2924','PMS-206','zaran'),('IBS-2992','PMS-273','zaran'),('IBS-4269','PBS-2613','munir'),('IBS-4280','PBS-2624','ibraheem'),('IBS-4400','KBS-6597.0','anayyat'),('IBS-4401','KBS-6458.0','tayyaba'),('KBS-2378.0','KMS-18.0','Jabir'),('KBS-2380.0','KMS-20.0','Nadeem'),('KBS-2386.0','KMS-26.0','M.'),('KBS-2391.0','KMS-31.0','Sheik'),('KBS-2395.0','KMS-35.0','Gulmina'),('KBS-2434.0','KMS-230.0','Munir'),('KBS-2483.0','KMS-280.0','Anwer'),('KBS-2509.0','KMS-322.0','Najum'),('KBS-2620.0','KMS-417.0','Alina'),('KBS-2621.0','KMS-418.0','Momd.'),('KBS-2630.0','KMS-427.0','Mustansar'),('KBS-2742.0','KMS-539.0','Abdul'),('KBS-2743.0','KMS-540.0','Rabia'),('KBS-2752.0','KMS-549.0','Haseeb'),('KBS-2791.0','KMS-588.0','Azra'),('KBS-2800.0','KMS-597.0','Sattar'),('KBS-2806.0','KMS-603.0','Daud'),('KBS-2807.0','KMS-604.0','Farheen'),('KBS-2808.0','KMS-605.0','Muhammad'),('KBS-2810.0','KMS-607.0','Abida'),('KBS-2813.0','KMS-610.0','Atra'),('KBS-2822.0','KMS-619.0','Rashid'),('KBS-2825.0','KMS-622.0','Masood'),('KBS-2907.0','KMS-704.0','Zaran'),('KBS-2913.0','KMS-710.0','Mohammed'),('KBS-2930.0','KMS-727.0','Fakhir'),('KBS-2935.0','KMS-732.0','Yaar'),('KBS-2950.0','KMS-747.0','Nabeera'),('KBS-2953.0','KMS-750.0','Huma'),('KBS-2954.0','KMS-751.0','Mehmood'),('KBS-2956.0','KMS-753.0','Murtaza'),('KBS-2966.0','KMS-763.0','Masood'),('KBS-2968.0','KMS-765.0','Momd.'),('KBS-2969.0','KMS-766.0','Tufail'),('KBS-2983.0','KMS-780.0','Sunera'),('KBS-3005.0','KMS-927.0','Amer'),('KBS-3013.0','KMS-935.0','Abdullah'),('KBS-3014.0','KMS-936.0','Ayesha'),('KBS-3058.0','KMS-980.0','Mouhammed'),('KBS-3080.0','KMS-1002.0','Saeed'),('KBS-3115.0','KMS-1036.0','Aneela'),('KBS-3116.0','KMS-1037.0','Zil-e-Ali'),('KBS-3118.0','KMS-1039.0','Tamania'),('KBS-3119.0','KMS-1040.0','Tipo'),('KBS-3120.0','KMS-1041.0','Mohammed'),('KBS-3137.0','KMS-1058.0','Abdul'),('KBS-3186.0','KMS-1108.0','Choudhary'),('KBS-3193.0','KMS-1115.0','Tayyaba'),('KBS-3196.0','KMS-1118.0','Aaila'),('KBS-3249.0','KMS-1171.0','Amna'),('KBS-3250.0','KMS-1172.0','Soneel'),('KBS-3300.0','KMS-1206.0','Kiran'),('KBS-3315.0','KMS-1221.0','Jan'),('KBS-3349.0','KMS-1255.0','Amjad'),('LBS-1600','LMS-37','Sehar'),('LBS-1601','LMS-38','Muazzam'),('LBS-1610','LMS-47','Jabbar'),('LBS-1623','LMS-60','Sh.'),('LBS-1628','LMS-65','Afeera'),('LBS-1635','LMS-72','Arsalan'),('LBS-1678','LMS-116','Sheikh'),('LBS-1736','LMS-174','Shakir'),('LBS-1854','LMS-276','Azhar'),('LBS-1907','LMS-328','Syyed'),('LBS-1981','LMS-403','Raza'),('LBS-2009','LMS-540','Hussain'),('LBS-2037','LMS-568','Daud'),('LBS-2047','LMS-578','Khurshid'),('LBS-2048','LMS-579','Najeeb'),('LBS-2058','LMS-589','Shamim'),('LBS-2148','LMS-663','Nafia'),('LBS-2152','LMS-667','Annan'),('LBS-2160','LMS-675','Mujeeb'),('PBS-1210','PMS-147','Karam'),('PBS-1234','PMS-171','Kamal');
/*!40000 ALTER TABLE `multiple_degrees` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-26  2:29:23
