CREATE DATABASE  IF NOT EXISTS `dwhlahore` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dwhlahore`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dwhlahore
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `cityname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES ('Westridge'),('Islamabad'),('Bagh'),('Bhimber'),('khuiratta'),('Kotli'),('Mangla'),('Mirpur'),('Muzaffarabad'),('Plandri'),('Rawat'),('Hasanabdal'),('Rawalakot'),('Punch'),('Amir Chah'),('Bazdar'),('Bela'),('Bellpat'),('Bagh'),('Burj'),('Chagai'),('Chah Sandan'),('Chakku'),('Kalar Saeedan'),('Saeedan'),('Chaman'),('Chhatr'),('Dalbandin'),('Dera Bugti'),('Dhana Sar'),('Diwana'),('Duki'),('Dushi'),('Duzab'),('Gajar'),('Gandava'),('Garhi Khairo'),('Garruck'),('Ghazluna'),('Girdan'),('Gulistan'),('Gwadar'),('Gwash'),('Hab Chauki'),('Hameedabad'),('Harnai'),('Hinglaj'),('Hoshab'),('Ispikan'),('Jhal'),('Jhal Jhao'),('Jhatpat'),('Jiwani'),('Kalandi'),('Kalat'),('Kamararod'),('Kanak'),('Kandi'),('Kanpur'),('Kapip'),('Kappar'),('Karodi'),('Katuri'),('Kharan'),('Khuzdar'),('Kikki'),('Kohan'),('Kohlu'),('Korak'),('Lahri'),('Lasbela'),('chishtian'),('Liari'),('Loralai'),('Mach'),('Mand'),('Manguchar'),('Mashki Chah'),('Maslti'),('Mastung'),('Mekhtar'),('Merui'),('Sibbi'),('Mianez'),('Murgha Kibzai'),('Musa Khel Bazar'),('Nagha Kalat'),('Nal'),('Naseerabad'),('Nauroz Kalat'),('Nur Gamma'),('Nushki'),('Nuttal'),('Ormara'),('Palantuk'),('Panjgur'),('Pasni'),('Piharak'),('Pishin'),('Qamruddin Karez'),('Qila Abdullah'),('Qila Ladgasht'),('Qila Safed'),('Qila Saifullah'),('Quetta'),('Rakhni'),('Robat Thana'),('Rodkhan'),('Saindak'),('Sanjawi'),('Saruna'),('Shabaz Kalat'),('Shahpur'),('Sharam Jogizai'),('Shingar'),('Shorap'),('Sibi'),('Sonmiani'),('Spezand'),('Spintangi'),('Sui'),('Suntsar'),('Surab'),('Thalo'),('Tump'),('Turbat'),('Umarao'),('pirMahal'),('Uthal'),('Vitakri'),('Wadh'),('Washap'),('Wasjuk'),('Yakmach'),('Zhob'),('Astor'),('Baramula'),('Hunza'),('Gilgit'),('Nagar'),('Skardu'),('Shangrila'),('Shandur'),('Bajaur'),('Hangu'),('Malakand'),('Miram Shah'),('Mohmand'),('Khyber'),('Kurram'),('North Waziristan'),('South Waziristan'),('Wana'),('Abbotabad'),('Ayubia'),('Adezai'),('Banda Daud Shah'),('Bannu'),('Batagram'),('Birote'),('Buner'),('Chakdara'),('Charsadda'),('Chitral'),('Dargai'),('Darya Khan'),('Dera Ismail Khan'),('Drasan'),('Drosh'),('Hangu'),('Haripur'),('Kalam'),('Karak'),('Khanaspur'),('Kohat'),('Kohistan'),('Lakki Marwat'),('Latamber'),('Lower Dir'),('Madyan'),('Malakand'),('Mansehra'),('Mardan'),('Mastuj'),('Mongora'),('Nowshera'),('Paharpur'),('Peshawar'),('Saidu Sharif'),('Shangla'),('Sakesar'),('Swabi'),('Swat'),('Tangi'),('Tank'),('Thall'),('Tordher'),('Upper Dir'),('Ahmedpur East'),('Ahmed Nager Chatha'),('Ali Pur'),('Arifwala'),('Attock'),('Basti Malook'),('Bhagalchur'),('Bhalwal'),('Bahawalnagar'),('Bahawalpur'),('Bhaipheru'),('Bhakkar'),('Burewala'),('Chailianwala'),('Chakwal'),('Chichawatni'),('Chiniot'),('Chowk Azam'),('Chowk Sarwar Shaheed'),('Daska'),('Darya Khan'),('Dera Ghazi Khan'),('Derawar Fort'),('Dhaular'),('Dina City'),('Dinga'),('Dipalpur'),('Faisalabad'),('Fateh Jang'),('Gadar'),('Ghakhar Mandi'),('Gujranwala'),('Gujrat'),('Gujar Khan'),('Hafizabad'),('Haroonabad'),('Hasilpur'),('Haveli Lakha'),('Jampur'),('Jhang'),('Jhelum'),('Kalabagh'),('Karor Lal Esan'),('Kasur'),('Kamalia'),('Kamokey'),('Khanewal'),('Khanpur'),('Kharian'),('Khushab'),('Kot Addu'),('Jahania'),('Jalla Araain'),('Jauharabad'),('Laar'),('Lahore'),('Lalamusa'),('Layyah'),('Lodhran'),('Mamoori'),('Mandi Bahauddin'),('Makhdoom Aali'),('Mandi Warburton'),('Mailsi'),('Mian Channu'),('Minawala'),('Mianwali'),('Multan'),('Murree'),('Muridke'),('Muzaffargarh'),('Narowal'),('Okara'),('Renala Khurd'),('Rajan Pur'),('Pak Pattan'),('Panjgur'),('Pattoki'),('Pirmahal'),('Qila Didar Singh'),('Rabwah'),('Raiwind'),('Rajan Pur'),('Rahim Yar Khan'),('Rawalpindi'),('Rohri'),('Sadiqabad'),('Safdar Abad � (Dhaban Singh)'),('Sahiwal'),('Sangla Hill'),('Samberial'),('Sarai Alamgir'),('Sargodha'),('Shakargarh'),('Shafqat Shaheed Chowk'),('Sheikhupura'),('Sialkot'),('Sohawa'),('Sooianwala'),('Sundar (city)'),('Talagang'),('Tarbela'),('Takhtbai'),('Taxila'),('Toba Tek Singh'),('Vehari'),('Wah Cantonment'),('Wazirabad'),('Ali Bandar'),('Baden'),('Chachro'),('Dadu'),('Digri'),('Diplo'),('Dokri'),('Gadra'),('Ghanian'),('Ghauspur'),('Ghotki'),('Hala'),('Hyderabad'),('Islamkot'),('Jacobabad'),('Jamesabad'),('Jamshoro'),('Janghar'),('Jati (Mughalbhin)'),('Jhudo'),('Jungshahi'),('Kandiaro'),('Karachi'),('Kashmor'),('Keti Bandar'),('Khairpur'),('Khora'),('Klupro'),('Khokhropur'),('Korangi'),('Kotri'),('Kot Sarae'),('Larkana'),('Lund'),('Mathi'),('Matiari'),('Mehar'),('Mirpur Batoro'),('Mirpur Khas'),('Mirpur Sakro'),('Mithi'),('Mithani'),('Moro'),('Nagar Parkar'),('Naushara'),('Naudero'),('Noushero Feroz'),('Nawabshah'),('Nazimabad'),('Naokot'),('Pendoo'),('Pokran'),('Qambar'),('Qazi Ahmad'),('Ranipur'),('Ratodero'),('Rohri'),('Saidu Sharif'),('Sakrand'),('Sanghar'),('Shadadkhot'),('Shahbandar'),('Shahdadpur'),('Shahpur Chakar'),('Shikarpur'),('Sujawal'),('Sukkur'),('Tando Adam'),('Tando Allahyar'),('Tando Bago'),('Tar Ahamd Rind'),('Thatta'),('Tujal'),('Umarkot'),('Veirwaro'),('Warah');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-26  9:42:27
