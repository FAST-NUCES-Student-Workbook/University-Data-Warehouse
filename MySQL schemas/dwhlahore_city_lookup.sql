CREATE DATABASE  IF NOT EXISTS `dwhlahore` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dwhlahore`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dwhlahore
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city_lookup`
--

DROP TABLE IF EXISTS `city_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_lookup` (
  `standard_city` varchar(50) DEFAULT NULL,
  `variation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_lookup`
--

LOCK TABLES `city_lookup` WRITE;
/*!40000 ALTER TABLE `city_lookup` DISABLE KEYS */;
INSERT INTO `city_lookup` VALUES ('bahawalpur','bahawalpur'),('khan','kohan'),('faisalabad','faisalabad'),('kharian','kharan'),('kharian','kharian'),('kharian','ghanian'),('wazirabad','hafizabad'),('wazirabad','sadiqabad'),('wazirabad','wazirabad'),('wazirabad','nazimabad'),('sargodha','sargodha'),('gujrawala','gujranwala'),('sahiwal','sahiwal'),('shekhopura','sheikhupura'),('gujrat','gujrat'),('bhawalpur','khanaspur'),('bhawalpur','bhagalchur'),('jehlum','jhelum'),('multan','multan'),('lalamusa','lalamusa'),('sialkot','sialkot'),('islamabad','isb'),('rawalpindi','rwp'),('rawalpindi','raw'),('rawalpindi','rawalpind'),('rawalpindi','rawalpin'),('abbotabad','abbottabad'),('karachi','khi'),('Dera Ghazi Khan','dg Khan'),('Dera Ghazi Khan','d.g. khan'),('hasanabdal','hasanabdal'),('peshawar','peshawar'),('haripur','haripur'),('haripur','hasilpur'),('haripur','khairpur'),('haripur','ranipur'),('noshera','nowshera'),('mardan','girdan'),('mardan','madyan'),('mardan','mardan'),('gilgit','gilgit'),('attock','attock'),('mansehra','mansehra'),('sawat','rawat'),('sawat','swat'),('abbotabad','abbotabad'),('islamabad','isb'),('rawalpindi','rwp'),('rawalpindi','raw'),('rawalpindi','rawalpind'),('rawalpindi','rawalpin'),('abbotabad','abbottabad'),('karachi','khi'),('Dera Ghazi Khan','dg Khan'),('Dera Ghazi Khan','d.g. khan');
/*!40000 ALTER TABLE `city_lookup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-26  9:42:28
